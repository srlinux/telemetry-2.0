#! /bin/bash
nmcli connection add type bridge autoconnect yes con-name br-leaf1-36 ifname br-leaf1-36
nmcli connection add type bridge autoconnect yes con-name br-leaf2-36 ifname br-leaf2-36
nmcli connection add type bridge autoconnect yes con-name br-leaf3-36 ifname br-leaf3-36
nmcli connection show
ip link set dev br-leaf1-36 mtu 9212
ip link set dev br-leaf2-36 mtu 9212
ip link set dev br-leaf3-36 mtu 9212
