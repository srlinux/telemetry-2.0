# Add IPv4
ip add add 172.31.1.2/24 dev eth1
ip ro add 172.31.0.0/16 via 172.31.1.1

# Add IPv6
ip -6 add add 2000::172:31:1:2/112 dev eth1
ip -6 add add 2000::172:31:1:3/112 dev eth1
ip -6 ro add 2000::172:31:0:0/96 via 2000::172:31:1:1
