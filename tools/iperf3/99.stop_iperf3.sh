#! /bin/bash

victims=$(ps -ef | grep iperf3 | awk '{print $2}')
for i in $victims; do
    echo $i
    kill -9 $i 2>/dev/null
done
