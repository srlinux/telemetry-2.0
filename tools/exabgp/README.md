# Put exabgp examples here

- Don't forget to add IPv4 and IPv6 addresses to a VETH interfaces
```
ip a add 192.168.1.2/24 dev srl-1
ip a add 2001::192:168:1:2/112 dev srl-1
```
