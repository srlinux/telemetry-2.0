# Put gobgp examples here
- For more information visit: https://github.com/osrg/gobgp

# Operations
- GoBGP operations could be logically devided into 2 steps
    1. Start GoBGP process, which establishes necessary BGP sessions (no prefix advertisment at this stage!)
        - Configuration file for GoBGP has 'toml' formatting. A few examples could be found in /tools/gobgp directory
        - Example:
        ```
        gobgpd -f simple.toml &
        ```
    2. Advertise prefixes by populating
        - All-in-one command could be used to populate RIB
            ```
            gobgp global rib add 10.0.0.1/32 \
                -a ipv4 \
                identifier 1 \
                origin igp \
                aspath 1,2,3 \
                nexthop $self \
                med 100 \
                local-pref 100 \
                community 1:1,2:2 \
                aigp metric 10 \
                large-community 111:111:111,222:222:222
            ```
        - As an alternative, a scripted approach can be used to populate multiple prefixes, sharing the same path attributes
            ```
            self_v6=2000::172:31:1:2

            attr_v6="
            -a ipv6 \
            identifier 1 \
            origin igp \
            aspath 1,2,3 \
            nexthop $self_v6 \
            "
 
            gobgp global rib add 2002::10:0:0:0/112 $attr_v6
            gobgp global rib add 2002::11:0:0:0/112 $attr_v6
            ```

