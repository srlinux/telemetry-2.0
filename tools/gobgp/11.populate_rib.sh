#! /bin/bash

self=192.168.1.2
# ---------------------
# Define BGP attributes
attr_v4="
 -a ipv4 \
 identifier 1 \
 origin igp \
 nexthop $self \
"

attr_v6="
 -a ipv6 \
 identifier 1 \
 origin igp \
 nexthop $self \
"
# -----------------

# Populate prefixes
gobgp global rib add 10.0.0.1/32 $attr_v4
gobgp global rib add 11.0.0.1/32 $attr_v4

gobgp global rib add 2002::10:0:0:0/112 $attr_v6
gobgp global rib add 2002::11:0:0:0/112 $attr_v6

# Report
echo -e "\n[INFO] show RIBv4: gobgp global rib -a ipv4"
gobgp global rib -a ipv4

echo -e "\n[INFO] show RIBv6: gobgp global rib -a ipv6"
gobgp global rib -a ipv6

echo -e "\n[INFO] show neighbors"
gobgp neighbor

echo "
[INFO] Nice commands to execute:
	gobgp global rib -a ipv4
	gobgp global rib -a ipv6
	gobgp neighbor 192.168.1.1 adj-out
"

exit
# Examples

gobgp global rib add 10.0.0.1/32 \
 -a ipv4 \
 identifier 1 \
 origin igp \
 aspath 1,2,3 \
 nexthop $self \
 med 100 \
 local-pref 100 \
 community 1:1,2:2 \
 aigp metric 10 \
 large-community 111:111:111,222:222:222

