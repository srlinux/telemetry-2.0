# Containerlab
- The following fabric is deployed using Containerlab project

    ![](images/topology-2.0.png)

- Phase 1: Create Linux bridges to interconnect SR Linux containers with Tools containers
    - Execute bash script
        ```sh
        ./00.create_networks.sh
        ```

        <details>
        <summary>Script content</summary>

        ```sh
        #! /bin/bash
        nmcli connection add type bridge autoconnect yes con-name br-leaf1-36 ifname br-leaf1-36
        nmcli connection add type bridge autoconnect yes con-name br-leaf2-36 ifname br-leaf2-36
        nmcli connection add type bridge autoconnect yes con-name br-leaf3-36 ifname br-leaf3-36
        nmcli connection show
        ip link set dev br-leaf1-36 mtu 9212
        ip link set dev br-leaf2-36 mtu 9212
        ip link set dev br-leaf3-36 mtu 9212
        ```
        </details>

- Phase 2: Deploy SR Linux fabric
    - Add SR Linux docker image to your system. Example:
        ```sh
        docker image load -i 20.6.2-332.tar.xz
        ```
    - Execute bash script
        ```sh
        ./01.deploy_srl.sh
        ```

        <details>
        <summary>Script content</summary>

        ```sh
        containerlab deploy -t topology_srl.yml
        ```
        </details>

        <details>
        <summary>Configuration file content</summary>

        ```yaml
		name: t2

		topology:
		  global_defaults:
			kind: srl
		  kinds:
			srl:
			  type: ixr6
			  config: /etc/containerlab/templates/srl/config.json
			  image: srlinux:20.6.2-332
			  license: license.key
			linux:
			  image: ubuntu-tools:latest
		  nodes:
			spine1:
			  kind: srl
			  config: ./srl_config/spine1.json
			spine2:
			  kind: srl
			  config: ./srl_config/spine2.json
			leaf1:
			  kind: srl
			  type: ixrd2
			  config: ./srl_config/leaf1.json
			leaf2:
			  kind: srl
			  type: ixrd2
			  config: ./srl_config/leaf2.json
			leaf3:
			  kind: srl
			  type: ixrd2
			  config: ./srl_config/leaf3.json
			br-leaf1-36:
			  kind: bridge
			br-leaf2-36:
			  kind: bridge
			br-leaf3-36:
			  kind: bridge
		  links:
			- endpoints: ["spine1:e1-1", "leaf1:e1-1"]
			- endpoints: ["spine1:e1-2", "leaf2:e1-1"]
			- endpoints: ["spine1:e1-3", "leaf3:e1-1"]
			- endpoints: ["spine2:e1-1", "leaf1:e1-2"]
			- endpoints: ["spine2:e1-2", "leaf2:e1-2"]
			- endpoints: ["spine2:e1-3", "leaf3:e1-2"]
			- endpoints: ["leaf1:e1-36", "br-leaf1-36:leaf1-36"]
			- endpoints: ["leaf2:e1-36", "br-leaf2-36:leaf2-36"]
			- endpoints: ["leaf3:e1-36", "br-leaf3-36:leaf3-36"]

        ```
        </details>
    > WARNING: Don't forget to put a license file to a project working directory!

- Phase 3: Deploy Tools containers
    - Add a linux container of your preference. Example:
        ```sh
        docker pull pklepikov/ubuntu-tools:latest
        ```
    - Execute bash script
        ```sh
        ./02.deploy_tools.sh
        ```

        <details>
        <summary>Script content</summary>

        ```sh
        containerlab deploy -t topology_tools.yml
        ```
        </details>

        <details>
        <summary>Configuration file content</summary>

        ```yaml
		name: tools

		topology:
		  kinds:
			linux:
			  image: ubuntu-tools:latest
		  nodes:
			tools-1:
			  kind: "linux"
			  binds:
				- /root/gitlab/telemetry-2.0/tools:/tools
			tools-2:
			  kind: "linux"
			  binds:
				- /root/gitlab/telemetry-2.0/tools:/tools
			br-leaf1-36:
			  kind: bridge
			br-leaf2-36:
			  kind: bridge
		  links:
			- endpoints: ["tools-1:eth1", "br-leaf1-36:tools-1"]
			- endpoints: ["tools-2:eth1", "br-leaf2-36:tools-2"]
        ```
        </details>
    > WARNING: Containerlab uses absolute path for directory bindings. Modify configuration file accordingly!

- Phase 4: Deploy Telemetry tools
    - Execute bash script
        ```sh
        ./03.start_telemetry_tools.sh
        ```

        <details>
        <summary>Script content</summary>

        ```sh
		#! /bin/bash

		user='0:0'
		docker run -d \
			--user $user \
			--name=telegraf \
			--net=host \
			-e HOST_PROC=/host/proc \
			-v /proc:/host/proc:ro \
			-v $(pwd)/telemetry/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
			telegraf

		docker run -d \
			--user $user \
			--name=prometheus \
			--net=host \
			-v $(pwd)/telemetry/prometheus.yml:/etc/prometheus/prometheus.yml \
			-v $(pwd)/telemetry/prometheus:/data/prometheus \
			prom/prometheus \
			--config.file="/etc/prometheus/prometheus.yml" \
			--storage.tsdb.path="/data/prometheus"

		docker run -d \
			--user $user \
			--net=host \
			--name=grafana \
			-v "$(pwd)/telemetry/grafana:/var/lib/grafana" \
			grafana/grafana
        ```
        </details>
    > WARNING/1: If you don't clone the project and simply reuse existing scripts, make sure that the following directories "`$(pwd)/telemetry/grafana`" and "`$(pwd)/telemetry/prometheus`" are present for the persistent Prometheus and Grafana storage.

    > WARNING/2: Verify correct access credentials for the "`$user`" to the "`$(pwd)/telemetry/grafana`" and "`$(pwd)/telemetry/prometheus`" directories, otherwise you can get issues to run containers.
- Phase 5: Run traffic
    - Login to Tools-1 container, which plays IPerf3 **server** role
        ```sh
        docker exec -ti clab-tools-tools-1 bash
        ```
        - Configure IPv4/IPv6 interfaces
            ```sh
            /tools/iperf3/01.server_ip_config.sh
            ```    
        - Start IPerf3 server
            ```sh
            iperf3 -s -p 5201 &
            iperf3 -s -p 5202 &
            ```        
    - Login to Tools-2 container, which plays IPerf3 **Client** role
        ```sh
        docker exec -ti clab-tools-tools-2 bash
        ```
        - Configure IPv4/IPv6 interfaces
            ```sh
            /tools/iperf3/02.client_ip_config.sh
            ```    
        - Start IPerf3 client
            ```sh
            iperf3 -c 2000::172:31:1:3 -t 10000 -i 10 -p 5202 -B 2000::172:31:2:3 -P 64 -b 2000000 &
            iperf3 -c 2000::172:31:1:2 -t 10000 -i 10 -p 5201 -B 2000::172:31:2:2 -P 64 -b 2000000 &
            ``` 
- SRLinux configuration
    - IPv6 BGP fabric used (full configuration of all elements is provided)
        <details>
        <summary>Interface configuration example (clickable)</summary>

        ```sh
        A:leaf1# info from running interface ethernet-1/1
            interface ethernet-1/1 {
                admin-state enable
                vlan-tagging true
                subinterface 1 {
                    ipv6 {
                        address 2002::100:64:1:0/127 {
                        }
                    }
                    vlan {
                        encap {
                            single-tagged {
                                vlan-id 1
                            }
                        }
                    }
                }
            }
        A:leaf1# info from running network-instance default interface ethernet-1/1.1
            network-instance default {
                interface ethernet-1/1.1 {
                }
            }
        ```
        </details>

        <details>
        <summary>BGP configuration example (clickable)</summary>

        ```sh
        A:leaf1# info from running network-instance default protocols bgp
            network-instance default {
                protocols {
                    bgp {
                        admin-state enable
                        autonomous-system 101
                        router-id 192.0.1.1
                        ebgp-default-policy {
                            import-reject-all false
                            export-reject-all false
                        }
                        group eBGPv6 {
                            export-policy export-local
                            ipv4-unicast {
                                admin-state enable
                                advertise-ipv6-next-hops true
                                receive-ipv6-next-hops true
                            }
                            ipv6-unicast {
                                admin-state enable
                            }
                        }
                        ipv4-unicast {
                            multipath {
                                allow-multiple-as true
                                max-paths-level-1 64
                                max-paths-level-2 64
                            }
                        }
                        ipv6-unicast {
                            multipath {
                                allow-multiple-as true
                                max-paths-level-1 64
                                max-paths-level-2 64
                            }
                        }
                        neighbor 2002::100:64:1:1 {
                            peer-as 201
                            peer-group eBGPv6
                        }
                        neighbor 2002::100:64:1:3 {
                            admin-state enable
                            peer-as 202
                            peer-group eBGPv6
                        }
                    }
                }
            }
        ```
        </details>
        
        <details>
        <summary>BGP verification (clickable)</summary>    

        ```sh
        A:leaf1# show network-instance default protocols bgp neighbor
        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        BGP neighbor summary for network-instance "default"
        Flags: S static, D dynamic, L discovered by LLDP, B BFD enabled, - disabled, * slow
        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        +---------------------+-------------------------------+---------------------+-------+-----------+-----------------+-----------------+---------------+-------------------------------+
        |      Net-Inst       |             Peer              |        Group        | Flags |  Peer-AS  |      State      |     Uptime      |   AFI/SAFI    |        [Rx/Active/Tx]         |
        +=====================+===============================+=====================+=======+===========+=================+=================+===============+===============================+
        | default             | 2002::100:64:1:1              | eBGPv6              | S     | 201       | established     | 0d:14h:33m:54s  | ipv4-unicast  | [2/2/1]                       |
        |                     |                               |                     |       |           |                 |                 | ipv6-unicast  | [2/2/1]                       |
        | default             | 2002::100:64:1:3              | eBGPv6              | S     | 202       | established     | 0d:14h:35m:29s  | ipv4-unicast  | [2/2/3]                       |
        |                     |                               |                     |       |           |                 |                 | ipv6-unicast  | [2/2/3]                       |
        +---------------------+-------------------------------+---------------------+-------+-----------+-----------------+-----------------+---------------+-------------------------------+
        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        Summary:
        2 configured neighbors, 2 configured sessions are established,0 disabled peers
        0 dynamic peers

        ```
        </details>
    - Client (iperf3) connections are distributed in the network using basic export policies
        <details>
        <summary>Policy configuration example (clickable)</summary>    

        ```sh
        A:leaf1# info from running routing-policy
            routing-policy {
                prefix-set local {
                    prefix 172.31.0.0/16 mask-length-range 16..32 {
                    }
                    prefix 2000::172:31:0:0/96 mask-length-range 96..128 {
                    }
                }
                policy export-local {
                    statement 10 {
                        match {
                            prefix-set local
                        }
                        action {
                            accept {
                            }
                        }
                    }
                }
            }
        ```
        </details>          
- SRLinux devices have to be configured with gnmi server and tls

    <details>
    <summary>GNMI server configuration (clickable)</summary>

    ```sh
    A:leaf1# info from running system gnmi-server
        system {
            gnmi-server {
                admin-state enable
                timeout 7200
                rate-limit 60
                session-limit 20
                commit-confirmed-timeout 0
                network-instance mgmt {
                    admin-state enable
                    use-authentication true
                    port 57400
                    tls-profile tls-containerlab-t-leaf1
                }
                unix-socket {
                    admin-state disable
                    use-authentication true
                }
            }
        }
    ```
    </details>

    <details>
    <summary>TLS configuration (clickable)</summary>

    ```sh
    A:leaf1# info from running system tls server-profile tls-containerlab-t-leaf1
        system {
            tls {
                server-profile tls-containerlab-t-leaf1 {
                    key $aes$aNVAlhD5KhtU=$C8c1R0zKlkWlMX3NRR7FE6Oe0hlbFrSrVBEjScI+ipkfKDQ9uwp24uuEu2R11WCrDi1L4PyqR1hmsdEucHRZjwcHb1D0TG6oqT5sL1fju6HRcYvCCDj+JRuzZSnRRzciBZCeqheM6GPT2SnD/Gr+OQELVE+1mOzmMsJbv1ZlQJxFNn9Ya5t2tPZmmR1vn1MYcg73+zexSHixwqgwH5r+GTqFIWISN+cMH3tD0RrRcH0BvJR8q/sjRDWFfQR8zBiZUy6JKGKmewxkio1RSXCAmuKKQH/X3Kfy6t6uso5RHhg5M2uwQgyyu3TOgDmSXDhKRgZHK0AVgUBkn7Y9rkYteq8qMy1rRnxmUYSZjME6+n/zcIid0SWiht0G32LkHoW4CMyJWrqju5JQb3k6qPt+/VvfRa/2Gkac6SeGyTQ1EB3sO7uUdKsR6tYY2Vw/UCsbRK8pC/Skn+1k3Yz2dmPOkfP99ixWdvAgM4vJmNd0DQz7eayZZlIBLTPD6mFbbTP2rpl/dG5bwvg0DuvzCYhJYWaN0y4HF4KgzmBAWjti9En0McP7+SXQkNCJrpetYvRQ3wsUQIC+78fMcd/L1taW7I9luVngfCapsot1jtWu6gc69bLk4+2KosrQj+ZT/hk48lriLJjEJtnWANrNokCxLi1LFlzTG5aO50X174FguGQKAINiiiEwEqByex/lG6bQeD7wJc5CWZ948J2d589oUbIfKuYoaAuZ2oHik2oMF1N7mFgD1aTanCOa786klKj+Bfyjbj8XvfDX9c8DmLUoMeDwEevKq8/mA/E2kzHvkODZ52m9PQvlB/dt6k902JcIW0qNTJ9NIP1fRC+hVF+80eal4U4+b2mQ0Dw8J+jmXZkerVaHzwKzlkzg9F+gdheVF/Omq+I9s9fH5csKSXm7XPfKFrfiwS784oUFKx4xPGRtAEfY6uWGrQvUm8QORj35F6DZ6CZycmCaeMVMzE48CQbjxEvaJTUJnhFR0QTjWcrcpAU55IiyNtYPr7PyCFj74/HyZIqIM1cJfiGWMmx7kfFksrLylO6W2BxCAvt9PS2XP62WzgLoRZqqlL4s6PRXi1o0ZA8prwSo2QFw7uqPfbF1qPoedu0rDY3pgzqeQbqiWYxAZit+mTL7L5M1+Fstkqc0IdelypB2B4FQULKiVer2Il5WvCoswe3ZRUktfMlGpoVzW+zRmR88VSrovCP72TlX1Te+Uf8fBYoLl2qrDhe0izVAv4YTavEimECIeWqFvqSfxybgGgl4cmQCK2lkCV3as20IlZK/wnDCkgj8ztGbZwJogvddWGEuw2ptpC3Esvmc4pxvxtZPO2CMCjuFED6lj5qvBJWop/+KvK0/OQrqbRMJcrC9SMI/o1hIERUSOiSJYFekugtoJUM7RHAlYhEiv7uWfoGRyFqrVfOWAOquVLfsnqThqCgng/6DYxAfC01AKsaB8D8+OCr8LkM/bOTII5xkvXUnVsyFDwc2E/65ltZdB9B79Bsq2cVwTRpUAxI6knGx06YiN4mrGkK7fcK3M+d/OQhYo8WTfUV5jWSKpFB7BtVODja3ePSrUuFv/dDxXlxbpcdBL4xd73DXk60wXLBjaMY5JJb9/RGMlGYdEoDpuoX71N9jDAVG/JtjVOZP5Ey+Bu15NEzRahxy+eCVkhvb8H3sbPk7egozflpsxQ94F2EXY9IorauBM9sqULegURBbIzLfid53eRgnoTF3PL68S1fYnXCxXCPOG979oeNfppwUfEfq716qXZj6MC+NydOvZrVW9n7Xo//zwhV8Bc3TG/jO5SpMEdBsTMCVSCWPOd49hZtu3bnB4jR+Mu/iDBxnQX5mkXyYpZHNu6jWj1Y5L/ynH8xacocU9I6uPIiEXGJliQ9co8yfSJoehvJ0/VUTYK8/Xy4Cg7jJ7J7VHEHOih8Yx89nF8YoFQeHxydmuZw1//fRtwWSy/UKXhErkQVe2yvAs1B5BPc/Ko568DZ1ARDLq6Jaje/pEiH97JEpT7nNIDHzEUXvpdKigXmKhq4bCN1dqB90cy5nSOp4iRjvl0kh/YaZj+rCxdZY7iS5OprmxM2QiUycWxKQdLb3u9R8jG0/Lax/m1ywOuyJZZjG10f+Yz2xHBkM1pxQIrMYOigRNBkuG17LR6NGxHT5eXcOpV3O4ycTwvVQBTNzfuS9fc2aTc/Ocy8ZBSY2lUMCu6e59lWbHvLv/QCRmKctNghEGNLm5temWSraErChJQlSBp5XuhSXOfxV0B2wksByH77CJ/8gzs1ycNZwFN/dkLsG6iYYvw2NIKnBkiEvRAkhjHY4FOAUa1e0X+Zv7/3B3wm7AD58G2z3XSVoCFo0mYIMoI/ZA90Gm6pea+gQ/8RhIXFXkztllvlmCc3Wmbv6AHmMWLsYWpudiXpCW2O/rKYzl1Zbes8Qca5QfsYhdTVCnG9h984eKF4KI2JLhR7n/BZUUfZAQxRYKLKcU45ext0HSXB9Tmqz9MI2S9ZX1UCeE1lt+1XfpTpDGNcDMNKcUF6z+d+wa0vT53/aXUY2uOsU/PajIAYwAnJxnmnjC9OSRfaC0R19bdANsQtOHFzJFKhlTkfaz7bStLaX7fbI5hkN0E2iqIK/Mtd1O+qh0mtVhE2zdO/53EM4PRXcvxaWRyDsjZ5ET8UGo6zOYhC1NnXc0C2Dr+VyCIXy5nZ64cAw2JY5zyLtZMDIDavEx2H6c4qDtE8sr2S6umnP2hvWBaYU32g5TazsSh69EjpmlI1cQ0g7HVKmvtI6PGoXCuyOgWd6QMJMlLDyk5Ju8AocsKnQOasVlNhA8NBK4kyyuC9zVAQx9KuBOHk33ZELCTWZTYe/WKoqQWxIgonBQ+a3X+2ax6hTjnKdbHnFdk7WTN/DeDjnwEqfmmsHLQTEWun8HhusLURUScxMl5xjcm5wKXm1nLBpafaS6IDsza1lwFq5/kX/8/jkozBVDGj/jB0UTdjzd+PGEZOi//xUTB7mxxdeAPThgTDp0NSMWsfpiZ+5KXMXVh+Ot5qn8NkTZAloTF17Fo8QfsmrhaZpWpOuYFs7kowBtCSOG5JTcLA0Ouxq0xZvmDJ8bQ6MOA/QKBwmLhu2wgNLwyhx0Wr7jNWIMf4uikhdxTATCUn+asQhPDPtoEcgAcKULZkOsTUv10p6GwDiIn97e7Jl8Mv+9IVPptVpfwO5A1Ql8esJ20xemwIhU/azO0DU3fx5EJS69V4/pzUXyL7VGz2qDuWG7YNIibVCqqHwV+X7JktlUwXAxtBZKIpalIIM1iHlMYQLLcUJXtBGu6b3xVd1+a3kxpEunJRNv7ZKX/M26XZ6M+toFKc/df3JEbJ6t1EWXJ9U8RPYTWQTCRxtnmA7UK40B6nnriRnoThhesU1VOMRx8bG68hth3aWHR4YYeuoSQ5imLJxRu7iwuMN2oExiWWkZ7Hqf8zM1+8szrxctYG5mzfHbzm09jHLxv9GqYKPNwEDbEtcJ/ok4nk/NfWPueM/oh6uU0oXnPo60UCD29tmJDhdKPDUbbKU//GzWQNY38nS2YkbsYHgwzQtBXRSfQKqumFlgz7wtDTBIFlvR0OwPdMNMs2Qte/plSU3Z7Jo0NzSeYsciKQFyY88F6VI0BAoMbG8GuJ8J9VUBnzms91WjR2UOkW5FJ+2eiOrnXzqmvI+YLCiMngKJT1I3fsvn4w1rLvKjRJEcvpfJrNB2AjqfHg9O+TLFTqAIa7yB6WUazTXe/KlL/V3lQw0Eug545muphX4mxJ2HlLdJb4raEmReEHB/FxwhNt9uwEHdkYbhc8aXY7YazRM3tvOAmO8esdsiX/88lbPtFO3q9R2dIJdkDmOeqiBU0ti3k28E0mDP9a1jPJ+QfqWCi8WwI3iuzy8TqenzRXijict4aLINDbIl0KozcEI2k16C9aLj8cjDLFUCuIq6R8791bgRxXNX5V4poPBuPV52JZRK6k/zUHj3MeVtn5Fs1G+PsgkjapHUpwZufX1OXnZDsImtoNMO+2ufWeDH7wLJG9vUksCyUSKeL6MoyFcXAsM/FOwFvr2ZG8DLQWk+jtFYnzwKbFqxyuBmZSBfQApoh6CZ3rwq4EdDgFL+CzWgMydLWsJ1KsRTvS+H3bXLF31CdOvbMYCNqdo5Y6Wi2CUOafhIOv19/gbwOinwo1FXj6bid08rHfNn9+BJh5McOB/ueDOWzSLTcXxoFBtx8SePI33WeQ7LFj4/b+8cCVAe6fEYYDOkj7crslaPQojEhYjBaB+I1jR7oaAzIBNSmdHqWq1eqRf3mzuz0S0/ZPekPSns244lV04VGbyK6d6/jKQJvFCyQqrUx4=
                    certificate "-----BEGIN CERTIFICATE-----
    MIIHMTCCBRmgAwIBAgICEAUwDQYJKoZIhvcNAQELBQAwcjELMAkGA1UEBhMCQkUx
    CzAJBgNVBAgMAkZMMQ4wDAYDVQQKDAVOb2tpYTEMMAoGA1UECwwDSU9OMQ8wDQYD
    VQQDDAZzdWJfY2ExJzAlBgkqhkiG9w0BCQEWGHBhdmVsLmtsZXBpa292QG5va2lh
    LmNvbTAeFw0yMDA5MjkwNzM1NTNaFw0zMDA5MjcwNzM1NTNaMIGUMQswCQYDVQQG
    EwJCRTELMAkGA1UECAwCRkwxEjAQBgNVBAcMCUFudHdlcnBlbjEOMAwGA1UECgwF
    Tm9raWExDDAKBgNVBAsMA0lPTjEdMBsGA1UEAwwUY29udGFpbmVybGFiLXQtbGVh
    ZjExJzAlBgkqhkiG9w0BCQEWGHBhdmVsLmtsZXBpa292QG5va2lhLmNvbTCCAiIw
    DQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAKHBcg6Hzlaw9nDY3/Y94LkbOq9H
    sqo3VH7umIsCoG2efHR2glB1iOhuOrHAakH/ueaXSNn5Zg26EehQLuxpDc5nesb1
    XriPEWMHwq3fD+ilhTn2qhcQ6ufFHJM7RRS7OICtBWPT08puzzpdJMrrvx12z7/h
    HDttqolpJa7Z/3ehVCLfGDADqju/xwka7h3/pLRSSdfaGFA92uM/lhrpI5fqYBFM
    vghKKbV5cE8uQYDyKs2/ABDF9fh0zxmaQ+kVfDWFygst+OsgEsE6v4HdGy3QSBbx
    KyAN4pI7ahhWSyVWNO63fi/wYzNUUjM1D4pfCnYPiBWkNRz1iSbkH8VlSoTURWkH
    ImO5GS80FSEF0RLICKRjBYl2ohEIhdSv9uRsL55SpCiLAMKBdEVsbV23SdMY7/Rv
    4CBtwYhvnE+GtSmL941XFVf8e6ljMEtrcO6LBJpQ/bbJ3E3u5TUOcKroNlyt0oz2
    34EEs8JKMjvwgIOK1BZ/MUuIK75yQJbdbG3smnCG2Uo0LSAMbEIAkpjTQBq1wwq9
    zaHECyZwlFZKNOKI+lWE7TGYawc4wQ5PywjI/O7G9p9mE3koSUpNzK1X2aMI/cxG
    /HkVgERF9JE+/5kbyTY/BFcw/lAZItaPbCISCU5YGg1h1bKGS/q9IX3k9B+3W95q
    HRB+i7bPRdDZjE+DAgMBAAGjggGsMIIBqDAJBgNVHRMEAjAAMBEGCWCGSAGG+EIB
    AQQEAwIGQDAzBglghkgBhvhCAQ0EJhYkT3BlblNTTCBHZW5lcmF0ZWQgU2VydmVy
    IENlcnRpZmljYXRlMB0GA1UdDgQWBBQ5AEabKCP9Kul+JsC8/3XhawDcnzCBtQYD
    VR0jBIGtMIGqgBTwSfelOEq/Sxga6YKtjnJ5AtsIz6GBjaSBijCBhzELMAkGA1UE
    BhMCQkUxCzAJBgNVBAgMAkZMMRIwEAYDVQQHDAlBbnR3ZXJwZW4xDjAMBgNVBAoM
    BU5va2lhMQwwCgYDVQQLDANJT04xEDAOBgNVBAMMB1Jvb3RfQ0ExJzAlBgkqhkiG
    9w0BCQEWGHBhdmVsLmtsZXBpa292QG5va2lhLmNvbYICEAAwDgYDVR0PAQH/BAQD
    AgWgMCcGA1UdJQQgMB4GCCsGAQUFBwMBBggrBgEFBQcDEQYIKwYBBQUHAwIwQwYD
    VR0RBDwwOocErBQTBIIcY29udGFpbmVybGFiLXQtbGVhZjEuaXBkLmxhYoIUY29u
    dGFpbmVybGFiLXQtbGVhZjEwDQYJKoZIhvcNAQELBQADggIBALKvUMXlHkbNt0qQ
    ng1LXoaqb16ikkFzZ9OczIjVYlHTnEpN35ZtHD1BYixsUdxTT4gbsdhf0G99M+3E
    TrzvfHjCHT7BQEXwlXK8YQ/iJF75Eu97DXLcTKlXjJcqP+lploFP9nXlkR92Blle
    TYN82W8bxJZeYLVr4HAh504wyPqyNXW0+oojETApi4qiBwXwU7O9B35Wg6pHSj0j
    wd9pXb6a7vcRh8De0BO59QQ5OvFXE5t1osuTcMSuTsU2hsyQmocJ40i0topTtlqy
    sEyz01WExtQZKbl4NNQ+7E1vwxxR9so0cEQBy5d8TcuSiFfzmmsL+4+drJnmOVgX
    TGYiAvBtQmHnLG2/Sy3n+DXDgABsyJnzs5Z5SVa53ZUac+ZZcMypUQP3EXzTN+Ui
    u9yTRUgCkWt/f1jUJMW3k61/ohAu+gfTIwjOAuxq7k1S95B+WDpQNlH8sa3e6lu8
    UJ+QG4jOwssRF9ZYIPsECOZNh+iBc3vNm2uoOr8Gc7U0tAcuungvWXOCzTVs8TUa
    bXHnRTTFwPxD4MXvccTgnhYzKe0S6fENseNiS5MlpkZX95czOs15yqfd87ORMjb2
    IZvjelcjn0z/JnnJ15BoJkRDvKn7OBNS4xmO5PNg2apmnKAyYSBrMP6YKqIGI4uq
    AhJPsyGOG6nfBvmBM0HE3EF5XCN3
    -----END CERTIFICATE-----"
                    authenticate-client false
                    trust-anchor "-----BEGIN CERTIFICATE-----
    MIIGATCCA+mgAwIBAgIUFjSq1GO1JJ0Tn/TEp+oQKX2/iUAwDQYJKoZIhvcNAQEL
    BQAwgYcxCzAJBgNVBAYTAkJFMQswCQYDVQQIDAJGTDESMBAGA1UEBwwJQW50d2Vy
    cGVuMQ4wDAYDVQQKDAVOb2tpYTEMMAoGA1UECwwDSU9OMRAwDgYDVQQDDAdSb290
    X0NBMScwJQYJKoZIhvcNAQkBFhhwYXZlbC5rbGVwaWtvdkBub2tpYS5jb20wHhcN
    MjAwOTE3MjAyMTE4WhcNNDAwOTEyMjAyMTE4WjCBhzELMAkGA1UEBhMCQkUxCzAJ
    BgNVBAgMAkZMMRIwEAYDVQQHDAlBbnR3ZXJwZW4xDjAMBgNVBAoMBU5va2lhMQww
    CgYDVQQLDANJT04xEDAOBgNVBAMMB1Jvb3RfQ0ExJzAlBgkqhkiG9w0BCQEWGHBh
    dmVsLmtsZXBpa292QG5va2lhLmNvbTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCC
    AgoCggIBAKTNwiSEgz6Oh6BXR7Y6R0yZpsXJocCVZxBlUqMpq7B+PcswqJGL3pwI
    4R2bMncCs7xKpPjtA1Z6ZF3wFaXXeYfYH1ayMd/emR31dLhMyCyf8OHLfRUQQGRA
    GRddvk0mtvOwQvGQimUSEIeIiz4uQnQHT/P5zTy6XqraETnbfLsoW01MmYlycThC
    CzBv9AUmvzDKhL/sHU6i/w1TgomQorxKprsKnnZQBoj9ZO/lA8gMVyl21tq/D/4w
    Pmd0YKUa8C22AWQiUaYfuc7b+4R5/JNwDilo6AW0GF5r8VDkIZLgSK9HwEOJYq4t
    AHM5FgnQfrsGSiEBfLk1aJXB5WtnjgZhVKI/vpXRABXMZKDiqNHTAK6f+iYE7pZ2
    8RCT/A/NGFC4XkAeMxsUwvs6zMFIjxHR/cz/C+AIkNcCOUFiaA2mQgMF2LXiLWX7
    OZTPtjnkBG/fcTniY/6yFvxxHsL8LILGN2lrhxQqdedybSVtVDKOWj1kzo5vhVg+
    5jrFQ3LlzbLFyMg2CvTWAbsKlrOD+WaByxt087jjOVGT5LY8C8P7gUxfNtb91dxC
    CntDNri6ZlH54wjNq54eU55p1+LEcsF4DYFjvuTTU5UYq9cqjZQtpBX3lrxmujPv
    df/PNq+tkDykW81fV7f1QfLGyvH7QMf9BfMWk5sZL4hsfW6h3uP7AgMBAAGjYzBh
    MB0GA1UdDgQWBBRGJpzTmU+15fMNlRQ9ew5tfFVnkzAfBgNVHSMEGDAWgBRGJpzT
    mU+15fMNlRQ9ew5tfFVnkzAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIB
    hjANBgkqhkiG9w0BAQsFAAOCAgEAO99eGvTzgAjCFu2uxltK/J0Q8cffgDZ9S3+7
    vY1UBzZwiQRm9ojLfn9HB0TuspDu6F4nwiSUQy+ayncgeOm9fevjx6FtTEdU0nK2
    iPOuZmrid3ZrO4RF5xqaygaSBvTZwxyl1OupoZRvZFF5M5UuVE2/YpvTgfhN0SMl
    r6ak9bUvEp2K31DCK+bfe/+ZzJDYdeUhZ5Q+9x03BZp3gj8kPocn828wr6U0ylYz
    MvaglMBaXIILixa1gxuph9a4kJPXWcJNvw3qEBHsq1LWE19aJWABMxA6vfz9u8RG
    o5zHE1AKm7mRowhhBukzGrSO8jkJVZ6yKhVcGcGNiqm0YURoQmdCCAEHFKte7Dc8
    RjszmXp0JZ8e0Fp6A5BfAbeEF80AacbBBv2Eo933ZXB7RYsp80qk2jDpyueexTyQ
    I35IC17jFopZ0a2udIkyXvPfV2CSZ/DzH56LVqJRSpoWhumSziUoDuPioVUTsfG3
    QUeluXz7bmqTW8ymDfQUYxCCaJ0UDOFGzahT8YUVJq+hEnFe7oJPZluXAkZkxvJq
    UFkoi4IciMfZ02pCfHlp5A2OxHVKh+khfwFKKcefHh5LLPJQA0smMVfM/JSFGwbw
    RMu7Zba5Oroj/ToRKBhhj89uGp76mcZLW/K8A5S1e+C4lbe5QotgDs0z5NBJP0Mq
    tUonvrI=
    -----END CERTIFICATE-----
    -----BEGIN CERTIFICATE-----
    MIIF3DCCA8SgAwIBAgICEAAwDQYJKoZIhvcNAQELBQAwgYcxCzAJBgNVBAYTAkJF
    MQswCQYDVQQIDAJGTDESMBAGA1UEBwwJQW50d2VycGVuMQ4wDAYDVQQKDAVOb2tp
    YTEMMAoGA1UECwwDSU9OMRAwDgYDVQQDDAdSb290X0NBMScwJQYJKoZIhvcNAQkB
    FhhwYXZlbC5rbGVwaWtvdkBub2tpYS5jb20wHhcNMjAwOTE3MjAyMTE5WhcNMzAw
    OTE1MjAyMTE5WjByMQswCQYDVQQGEwJCRTELMAkGA1UECAwCRkwxDjAMBgNVBAoM
    BU5va2lhMQwwCgYDVQQLDANJT04xDzANBgNVBAMMBnN1Yl9jYTEnMCUGCSqGSIb3
    DQEJARYYcGF2ZWwua2xlcGlrb3ZAbm9raWEuY29tMIICIjANBgkqhkiG9w0BAQEF
    AAOCAg8AMIICCgKCAgEAvIS/HECMf0BJgJE6p4/e6W/qyw8dEhVpeQ4pnWXEgXI0
    v3s/5FbXCfMy3oeDwVkDM4gSXSgn5x+WmgSYRuCYPm2V4Zgy7pU2e0bTh9c4LCgC
    jsdoxEj9E85dFTYRIv1p73Im6kpJg8K9kSJnN6q7n6LkclO7TxoIbW6PwpNkBrvp
    nh9CXMyqmccSil1NGmdIesKf3tcBriQ3YvGPo23ggukv5tpuFVVH9vkm/wik0n3z
    6EXpihgJBFcI1C1c3PnIMooQemGIT7jLoj8ijfFcfs6lJdTgDPl3GCUwLFC6JkWg
    oIlaQCVXxshiWqEsG2J6NRcEjd6gs6loOV/fZ2hqp3Ah1Kn5kr+47vTr7SHzVG81
    9oDXKO8MUyKov02iab3J3m7MagJWjGfq+nCxFffnke6t4fZRpRKMGiLe3maaCH1n
    INwXxTdPsj+gysrfi259BFNh94DGtrgj2E5aE3CqH1ZY6HQ6qzMX7MJah+hJXHR1
    DGL5hT/7FT0bBerdzOo5VQarSvbYDbB8zucG3z2ja0YTMN7hetuDY268R6Ryalfm
    rGMGY06umFZRnbLc+uE1/035UkF2k7oSpKFakRShX5hMhtXBMQukpG8FELaBM3Pg
    oOsy8a7qB9xoUAhhOvb5MoPM2WBigoM0AwVxKmNFfmPJNeVrXOK948Tw/pPzWN0C
    AwEAAaNmMGQwHQYDVR0OBBYEFPBJ96U4Sr9LGBrpgq2OcnkC2wjPMB8GA1UdIwQY
    MBaAFEYmnNOZT7Xl8w2VFD17Dm18VWeTMBIGA1UdEwEB/wQIMAYBAf8CAQAwDgYD
    VR0PAQH/BAQDAgGGMA0GCSqGSIb3DQEBCwUAA4ICAQAHRCq+lQcDa/KgOWU5OyoP
    KfrzYeZpAt9EXMsKIjG3+mx7AwYULpcnshgVZ3gGoDpC5+RLvQhzMPP+t+fb2Pxn
    NFlYcRkeaFv7yt5UmzaMRDNHo43McuNSbau5Cmtpg4sh0BVDPujCNqF2iI96TBjX
    JtGV1ntS8/3TGyX1n+PB9YryHjJPloxf0r4Xj98NRwUfJQCLrX/24yEPak2sjjsp
    7XO1ngjrdwgUv7rktSncrirFj0nU0rCCUA6nLAZ+OPAcWRzhozKzhk174gfT2Lqc
    eS/dCUW34N1Hoh++2vhJmXp9OV6/6IzZtZenEk78pw4DqJPAimjq0HYX1PHCwhOY
    ZZrXOwz4BtHyYT/jt8fUdvL+N5bN3GJumOlQlFgMoAIY6/LKteZL1XBwy7vgwoGI
    HK6hFn32WJHEZXxw4FZE6lGgjbnVT8dieDzVtOpsAMJx/783kaFZO+F47/pgfHLu
    K7eD2+TQTQgpNCaGwMb3ypyDPYq9o+fqDQyPfDzAMolxgqnvtldoIUTJiM5cTnj+
    Pa1pHTTOCFwbIm7hM96CzanMzBo+XbgsPot6KKutKnFZlxOubRaB8ZLF1RR1cr2W
    eEgaQUKCLaJG7K+kTyih60c7a+rwk9feQMdltCDZL06jrz5Qgo6e730vkCaOvhTu
    CuCdnkncbWwLXi4iOEOopg==
    -----END CERTIFICATE-----"
                }
            }
        }
    ```
    </details>
