# Telemetry: SRL local CLI
- 'info from state' command demonstrates 'one time' subscription (mode=once) 

    ```sh
    A:leaf1# info from state / network-instance default protocols bgp neighbor 2002::100:64:1:1 received-messages
        network-instance default {
            protocols {
                bgp {
                    neighbor 2002::100:64:1:1 {
                        received-messages {
                            queue-depth 0
                            total-updates 6
                            total-non-updates 5783
                            malformed-updates 0
                            total-messages 5789
                        }
                    }
                }
            }
        }
    ```
- Local SRL subscription
    - 'monitor' command gives a possibility to make periodic (mode=stream,sample) or on-change (mode=stream,on-change) subscription to any state
    - 'monitor' command has several options
        ```sh
        A:leaf1# monitor
        usage: monitor [from <running|state>] [recursive] [sample <value>] [updates-only]

        Interactively subscribe to state changes

        Named arguments:
        from              Datastore to retrieve data from
        recursive         Recursively show all changed to children as well
        sample            Use sampling instead of on change monitoring (interval in seconds)
        updates-only      Show only the updates (not the initial sync)
        ```
    - Example:
        ```sh
        A:leaf1# monitor / network-instance default protocols bgp neighbor 2002::100:64:1:1 received-messages
        [2020-10-08 20:59:02.441934]: update /network-instance[name=default]/protocols/bgp/neighbor[peer-address=2002::100:64:1:1]/received-messages
        [2020-10-08 20:59:02.442129]: update /network-instance[name=default]/protocols/bgp/neighbor[peer-address=2002::100:64:1:1]/received-messages/queue-depth:0
        [2020-10-08 20:59:02.442235]: update /network-instance[name=default]/protocols/bgp/neighbor[peer-address=2002::100:64:1:1]/received-messages/total-updates:6
        [2020-10-08 20:59:02.442308]: update /network-instance[name=default]/protocols/bgp/neighbor[peer-address=2002::100:64:1:1]/received-messages/total-non-updates:5786
        [2020-10-08 20:59:02.442367]: update /network-instance[name=default]/protocols/bgp/neighbor[peer-address=2002::100:64:1:1]/received-messages/malformed-updates:0
        [2020-10-08 20:59:02.442422]: update /network-instance[name=default]/protocols/bgp/neighbor[peer-address=2002::100:64:1:1]/received-messages/total-messages:5792
        ```
