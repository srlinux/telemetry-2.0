# Telemetry: Telegraf/Prometheus/Grafana
- In order to demonstrate a Grafana dashboard with a nice graphical statistics, we have to deploy a a few background systems to collect and store telemetry data:
    - Telegraf
    - Prometheus
    - Grafana
    
![image](images/grafana.jpg)
## Deployment
- All 3 systems are deployed as a docker container.
    > Note: User 'telemetry' is used. All permissions should be granted during "host preparation phase"

    ```sh
    user='978:977'
    docker run -d \
        --user $user \
        --name=telegraf \
        --net=host \
        -e HOST_PROC=/host/proc \
        -v /proc:/host/proc:ro \
        -v $(pwd)/telemetry/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
        telegraf

    docker run -d \
        --user $user \
        --name=prometheus \
        --net=host \
        -v $(pwd)/telemetry/prometheus.yml:/etc/prometheus/prometheus.yml \
        -v $(pwd)/telemetry/prometheus:/data/prometheus \
        prom/prometheus \
        --config.file="/etc/prometheus/prometheus.yml" \
        --storage.tsdb.path="/data/prometheus"

    docker run -d \
        --user $user \
        --net=host \
        --name=grafana \
        -v "$(pwd)/telemetry/grafana:/var/lib/grafana" \
        -p 3000:3000 \
        grafana/grafana
    ```
    - Key takeaways:
        - Each container has a mount directory to make configuration persistent and preserve DB data

- Verification
    ```sh
    # docker ps
    CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS               NAMES
    b93f9ef6d35f        grafana/grafana      "/run.sh"                8 minutes ago       Up 8 minutes                            grafana
    3fdb9d7c0e58        prom/prometheus      "/bin/prometheus --c…"   8 minutes ago       Up 8 minutes                            prometheus
    7415522aa197        telegraf             "/entrypoint.sh tele…"   8 minutes ago       Up 8 minutes                            telegraf
    ```

## Telegraf
- Telegraf is used to collect telemetry data using gNMI

    <details>
    <summary>telegraf.conf (clickable)</summary>

    ```sh
    # cat telemetry/telegraf.conf
    [global_tags]
    [agent]
    interval = "10s"
    round_interval = true
    metric_batch_size = 1000
    metric_buffer_limit = 10000
    collection_jitter = "0s"
    flush_interval = "10s"
    flush_jitter = "0s"
    precision = ""
    hostname = ""
    omit_hostname = false
    #[[outputs.influxdb]]
    [[outputs.prometheus_client]]
        listen = "0.0.0.0:9273"
        metric_version = 2
        path = "/metrics"
        expiration_interval = "180s"
        string_as_label = false
        export_timestamp = true
    [[processors.converter]]
    [processors.converter.tags]
        string = []
        integer = []
        unsigned = []
        boolean = []
        float = []
    #  [processors.converter.fields]
    #    tag = []
    #    string = []
    #    integer = []
    #    unsigned = ['*frames*', '*packets*', '*octets*', '*bps', '*active-entries-with-ecmp*', '*active-entries*', '*route*', '*peer*', '*path*', '*prefix*']
    #    boolean = []
    #    float = []
    [processors.converter.fields]
        tag = []
        string = []
        integer = ['*fragments.received', '*packets', '*bytes', '*octets', '*misses', '*errors', '*drops', '*mismatches', '*fragments*', '*encrypted', '*decrypted', '*size', '*use', '*memory', '*memory_free', '*memory_physical', '*memory_reserved', '*total-paths', '*total-prefixes', '*routes']
        unsigned = []
        boolean = []
        float = ['*cpu-usage', '*usage', '*count', '*static.lan.to.lan', '*dynamic.lan.to.lan', '*remote.access', '*total', '*static.ipsec.tunnels', '*dynamic.ipsec.tunnels']

    [[inputs.gnmi]]
    addresses = ["clab-t2-leaf1:57400","clab-t2-leaf2:57400","clab-t2-leaf3:57400","clab-t2-spine1:57400","clab-t2-spine2:57400"]
    name_prefix = "SRL_"
    username = "admin"
    password = "admin"
    encoding = "json_ietf"
    redial = "10s"
    enable_tls = true
    tls_ca = ""
    insecure_skip_verify = true
    [inputs.gnmi.aliases]
        interface = "/srl_nokia-interfaces:interface"
        networkinstance = "/srl_nokia-network-instance:network-instance"
    [[inputs.gnmi.subscription]]
        name = "interface"
        origin = "srl_nokia-interfaces"
        path = "/interface[name=ethernet-1/*]/"
        subscription_mode = "sample"
        sample_interval = "5s"
    [[inputs.gnmi.subscription]]
        # gnmic --config gnmic_subscription.yaml subscribe --path /platform/control[slot=*]/cpu[index=all]
        name = "platform_cpu"
        origin = "srl_nokia_cpu"
        path = "/platform/control[slot=*]/cpu[index=all]"
        subscription_mode = "sample"
        sample_interval = "5s"
    [[inputs.gnmi.subscription]]
        # gnmic --config gnmic_subscription.yaml subscribe --path /platform/control[slot=A*]/memory
        name = "platform_memory"
        origin = "srl_nokia_memory"
        path = "/platform/control[slot=*]/memory/"
        subscription_mode = "sample"
        sample_interval = "5s"

    [[inputs.gnmi]]
    addresses = ["clab-t2-leaf1:57400","clab-t2-leaf2:57400","clab-t2-leaf3:57400","clab-t2-spine1:57400","clab-t2-spine2:57400"]
    name_prefix = "SRL_"
    username = "admin"
    password = "admin"
    encoding = "json_ietf"
    redial = "10s"
    enable_tls = true
    tls_ca = ""
    insecure_skip_verify = true
    [inputs.gnmi.aliases]
        interface = "/srl_nokia-interfaces:interface"
        networkinstance = "/srl_nokia-network-instance:network-instance"
    [[inputs.gnmi.subscription]]
        name = "network-instance_protocols_bgp"
        origin = "srl_nokia_bgp"
        path = "/network-instance[name=*]/protocols/bgp/statistics"
        subscription_mode = "on_change"
    [[inputs.gnmi.subscription]]
        name = "network-instance"
        origin = "srl_nokia-network-instance"
        path = "/network-instance[name=*]/route-table/ipv4-unicast/statistics/"
        subscription_mode = "on_change"
    [[inputs.gnmi.subscription]]
        name = "network-instance"
        origin = "srl_nokia-network-instance"
        path = "/network-instance[name=*]/route-table/ipv6-unicast/statistics/"
        subscription_mode = "on_change"

    [[inputs.gnmi]]
    addresses = ["clab-t2-leaf1:57400","clab-t2-leaf2:57400","clab-t2-leaf3:57400","clab-t2-spine1:57400","clab-t2-spine2:57400"]
    name_prefix = "SRL_"
    username = "admin"
    password = "admin"
    encoding = "json_ietf"
    redial = "10s"
    enable_tls = true
    tls_ca = ""
    insecure_skip_verify = true
    [inputs.gnmi.aliases]
        interface = "/srl_nokia-interfaces:interface"
        networkinstance = "/srl_nokia-network-instance:network-instance"
    [[inputs.gnmi.subscription]]
        name = "network-instance_protocols_bgp"
        origin = "srl_nokia_bgp"
        path = "/network-instance[name=*]/protocols/bgp/statistics"
        subscription_mode = "sample"
        sample_interval = "1m"
    [[inputs.gnmi.subscription]]
        name = "network-instance"
        origin = "srl_nokia-network-instance"
        path = "/network-instance[name=*]/route-table/ipv4-unicast/statistics/"
        subscription_mode = "sample"
        sample_interval = "1m"
    [[inputs.gnmi.subscription]]
        name = "network-instance"
        origin = "srl_nokia-network-instance"
        path = "/network-instance[name=*]/route-table/ipv6-unicast/statistics/"
        subscription_mode = "sample"
        sample_interval = "1m"

    ```

- SRLinux subcription verification
    - Option 1:
        ```sh
        A:leaf1# info from state / system gnmi-server subscription * | as table | filter fields mode remote-host start-time user user-agent sample-interval
        +------------+-----------------------------+-----------------------------+-----------------------------------------+----------------------+----------------+-----------------------------+
        |     Id     |            User             |         User-agent          |               Remote-host               |   Sample-interval    |      Mode      |         Start-time          |
        +============+=============================+=============================+=========================================+======================+================+=============================+
        |          1 | admin                       | grpc-go/1.28.0              | 172.20.19.1                             |                      | ON_CHANGE      | 2020-10-09T20:04:56.443Z    |
        |          2 | admin                       | grpc-go/1.28.0              | 172.20.19.1                             |                   60 | SAMPLE         | 2020-10-09T20:04:56.444Z    |
        |          3 | admin                       | grpc-go/1.28.0              | 172.20.19.1                             |                    5 | SAMPLE         | 2020-10-09T20:04:56.458Z    |
        +------------+-----------------------------+-----------------------------+-----------------------------------------+----------------------+----------------+-----------------------------+
        ```
    - Option 2:
        ```sh
        A:leaf1# info from state / system gnmi-server subscription *
            system {
                gnmi-server {
                    subscription 1 {
                        user admin
                        user-agent grpc-go/1.28.0
                        remote-host 172.20.19.1
                        remote-port 39908
                        mode ON_CHANGE
                        start-time 2020-10-09T20:04:56.443Z
                        paths [
                            "network-instance[name=*]/protocols/bgp/statistics/..."
                            "network-instance[name=*]/route-table/ipv4-unicast/statistics/..."
                            "network-instance[name=*]/route-table/ipv6-unicast/statistics/..."
                        ]
                    }
                    subscription 2 {
                        user admin
                        user-agent grpc-go/1.28.0
                        remote-host 172.20.19.1
                        remote-port 39896
                        sample-interval 60
                        mode SAMPLE
                        start-time 2020-10-09T20:04:56.444Z
                        paths [
                            "network-instance[name=*]/protocols/bgp/statistics/..."
                            "network-instance[name=*]/route-table/ipv4-unicast/statistics/..."
                            "network-instance[name=*]/route-table/ipv6-unicast/statistics/..."
                        ]
                    }
                    subscription 3 {
                        user admin
                        user-agent grpc-go/1.28.0
                        remote-host 172.20.19.1
                        remote-port 39906
                        sample-interval 5
                        mode SAMPLE
                        start-time 2020-10-09T20:04:56.458Z
                        paths [
                            "interface[name=ethernet-1/*]/..."
                            "platform/control[slot=*]/cpu[index=all]/..."
                            "platform/control[slot=*]/memory/..."
                        ]
                    }
                }
            }
        ```

- Key takeaways:
    - multiple "inputs.gnmi" are possible
    - same subscription XPath can be used in different subscribtions
    - different subscription modes are supported for the same XPath
        - for example BGP stats can be collected in a 'sample' mode using quite big sample interval _AND_ at the same time 'on-change' mode can be used for immediate statistics collection
    - section "processors.converter.fields" is required for a proper format convertion
        - if statistics is not seen by telegraf:
            - check if subscription is active on the node
            - check the format of data
    - pay attention to encoding: 
        - SRL supports json_ietf
        - SR-OS supports json
 
## Prometheus    
- Prometheus is a TSDB to keep statistics persistency
    - Periodically pulls data from Telegraf

    <details>
    <summary>prometheus.yml (clickable)</summary>

    ```yml
    # cat telemetry/prometheus.yml
    # my global config
    global:
    scrape_interval:     5s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
    evaluation_interval: 5s # Evaluate rules every 15 seconds. The default is every 1 minute.
    # scrape_timeout is set to the global default (10s).

    # Alertmanager configuration
    alerting:
    alertmanagers:
    - static_configs:
        - targets:
        # - alertmanager:9093

    # Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
    rule_files:
    # - "first_rules.yml"
    # - "second_rules.yml"

    # A scrape configuration containing exactly one endpoint to scrape:
    # Here it's Prometheus itself.
    scrape_configs:
    # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
    - job_name: 'prometheus'

        # metrics_path defaults to '/metrics'
        # scheme defaults to 'http'.

        static_configs:
        - targets: ['localhost:9090']

    - job_name: 'telegraf_swiss'
        static_configs:
        - targets: ['localhost:9273']
    ```

## Grafana

![](images/grafana-2.0.png)
- WeatherMap part is built using FlowCharting (https://grafana.com/grafana/plugins/agenty-flowcharting-panel)
    - Installation: packages was simply uploaded to `$(pwd)/telemetry/grafana/plugins/` directory and unzipped. Container is restarted.
- Other dashboard components are standard ones
