# Telemetry: gNMIc tool
'gnmic' is a gNMI CLI client that provides full support for Capabilities, Get, Set and Subscribe RPCs with collector capabilities.
  > More information here: https://gnmic.kmrd.dev/

## gNMIc configuration
- Minimal configuration file. Inspired by https://github.com/karimra/gnmic/blob/master/config.yaml
  > file: [gnmic_subscription.yaml](gnmic_subscription.yaml)
  
    ```yml
    targets:
    #  clab-t2-spine1:
      clab-t2-leaf1:
        subscriptions:
    #      - system_facts
          - port_stats
    #      - service_state
          - bgp_neighbor_state

    username: admin
    password: admin
    port: 57400
    timeout: 10s
    tls-cert: certs/client.cert.pem
    tls-ca: certs/ca.cert.pem
    skip-verify: false
    #skip-verify: true
    encoding: json_ietf
    #debug: true
    no-prefix: true
    ```

## gNMIc examples
### Single XPath subscription
- Subscribe command
    ```sh
    gnmic \
        --config gnmic_subscription.yaml \
        subscribe \
        --path /network-instance[name=*]/protocols/bgp/neighbor[peer-address=*]/received-messages
    ```
    <details>
    <summary>Click to see output</summary>    
        
    ```json
    # gnmic --config gnmic_subscription.yaml subscribe --path /network-instance[name=*]/protocols/bgp/neighbor[peer-address=*]/received-messages
        
        {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309231685332,
          "time": "2020-10-08T23:08:29.231685332+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:1:0]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5819",
                  "total-non-updates": "5807",
                  "total-updates": "12"
                }
              }
            }
          ]
        }
        {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309232422682,
          "time": "2020-10-08T23:08:29.232422682+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:1]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5827",
                  "total-non-updates": "5817",
                  "total-updates": "10"
                }
              }
            }
          ]
        }
        {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309233279016,
          "time": "2020-10-08T23:08:29.233279016+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:3]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5822",
                  "total-non-updates": "5816",
                  "total-updates": "6"
                }
              }
            }
          ]
        }
        
        {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309233513344,
          "time": "2020-10-08T23:08:29.233513344+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:0]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5808",
                  "total-non-updates": "5800",
                  "total-updates": "8"
                }
              }
            }
          ]
        }
        {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309234100903,
          "time": "2020-10-08T23:08:29.234100903+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:3:0]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5806",
                  "total-non-updates": "5802",
                  "total-updates": "4"
                }
              }
            }
          ]
        }
        
    ```
###  Multiple XPath subscriptions
- Multiple XPath subscriptions are possible if 'subscriptions' container is defined in the configuration file
  - Here is an examples of different subscription modes (more information could be found on the official project page)
    - stream/sample
    - stream/on-change
    - once
    ```yml
      # SRLinux subscription container
      subscriptions:
          port_stats:
              paths:
                - "/interface[name=ethernet-1/1]/traffic-rate"
                - "/interface[name=ethernet-1/2]/traffic-rate"
              mode: stream
              stream-mode: sample
              sample-interval: 5s
              encoding: json_ietf
          bgp_neighbor_state:
              paths:
                - "network-instance[name=default]/protocols/bgp/neighbor[peer-address=200*]/admin-state"
              mode: stream
              stream-mode: on-change
          service_state:
              paths:
                - "/network-instance[name=*]/admin-state"
              mode: stream
              stream-mode: on-change
          system_facts:
              paths:
                - "/system/name/host-name"
                - "/platform/control[slot=*]/software-version"
              mode: once
    ```
  - Then a 'subscriptions' could be allocated for a particular target(s) usign the following format: 
      ```yml
      targets:
        clab-t2-spine1:
        clab-t2-leaf1:
          subscriptions:
            - system_facts
            - port_stats
            - service_state
            - bgp_neighbor_state

      ```
      <details>
      <summary>Click to see full configuration</summary>
      
      ```yml
        # cat gnmic_subscription.yaml
        # Inspired by https://github.com/karimra/gnmic/blob/master/config.yaml

        targets:
          containerlab-t-spine1:
          containerlab-t-leaf1:
              subscriptions:
              - system_facts
              - port_stats
              - service_state
              - bgp_neighbor_state

        username: admin
        password: admin
        port: 57400
        timeout: 10s
        tls-cert: certs/client.cert.pem
        tls-ca: certs/ca.cert.pem
        #skip-verify: false
        skip-verify: true
        encoding: json_ietf
        #debug: true
        no-prefix: true

        # SRLinux subscription container
        subscriptions:
          port_stats:
              paths:
                - "/interface[name=ethernet-1/1]/traffic-rate"
                - "/interface[name=ethernet-1/2]/traffic-rate"
              mode: stream
              stream-mode: sample
              sample-interval: 5s
              encoding: json_ietf
          bgp_neighbor_state:
              paths:
                - "network-instance[name=default]/protocols/bgp/neighbor[peer-address=200*]/admin-state"
              mode: stream
              stream-mode: on-change
          service_state:
              paths:
                - "/network-instance[name=*]/admin-state"
              mode: stream
              stream-mode: on-change
          system_facts:
              paths:
                - "/system/name/host-name"
                - "/platform/control[slot=*]/software-version"
              mode: once
      ```  
      </details>

  - Start subscription
      ``` 
      gnmic --config gnmic_subscription.yaml subscribe 
      ```
      <details>
      <summary>Click to see output</summary>    
          
      ```json
				# gnmic --config gnmic_subscription.yaml subscribe

				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "service_state",
				  "timestamp": 1606759744313687039,
				  "time": "2020-11-30T19:09:04.313687039+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=mgmt]",
					  "values": {
						"srl_nokia-network-instance:network-instance": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "service_state",
				  "timestamp": 1606759744313335497,
				  "time": "2020-11-30T19:09:04.313335497+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=default]",
					  "values": {
						"srl_nokia-network-instance:network-instance": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "port_stats",
				  "timestamp": 1606759744314021319,
				  "time": "2020-11-30T19:09:04.314021319+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-interfaces:interface[name=ethernet-1/1]/traffic-rate",
					  "values": {
						"srl_nokia-interfaces:interface/traffic-rate": {
						  "in-bps": "133283693",
						  "out-bps": "4082987"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "port_stats",
				  "timestamp": 1606759744314496965,
				  "time": "2020-11-30T19:09:04.314496965+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-interfaces:interface[name=ethernet-1/1]/traffic-rate",
					  "values": {
						"srl_nokia-interfaces:interface/traffic-rate": {
						  "in-bps": "4084091",
						  "out-bps": "133310324"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "service_state",
				  "timestamp": 1606759744314230562,
				  "time": "2020-11-30T19:09:04.314230562+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=default]",
					  "values": {
						"srl_nokia-network-instance:network-instance": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "service_state",
				  "timestamp": 1606759744314381335,
				  "time": "2020-11-30T19:09:04.314381335+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=mgmt]",
					  "values": {
						"srl_nokia-network-instance:network-instance": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}

				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "port_stats",
				  "timestamp": 1606759744316515994,
				  "time": "2020-11-30T19:09:04.316515994+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-interfaces:interface[name=ethernet-1/2]/traffic-rate",
					  "values": {
						"srl_nokia-interfaces:interface/traffic-rate": {
						  "in-bps": "137514917",
						  "out-bps": "4965590"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "bgp_neighbor_state",
				  "timestamp": 1606759744317697767,
				  "time": "2020-11-30T19:09:04.317697767+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:1:0]",
					  "values": {
						"srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "port_stats",
				  "timestamp": 1606759744318175327,
				  "time": "2020-11-30T19:09:04.318175327+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-interfaces:interface[name=ethernet-1/2]/traffic-rate",
					  "values": {
						"srl_nokia-interfaces:interface/traffic-rate": {
						  "in-bps": "133310165",
						  "out-bps": "4083932"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "system_facts",
				  "timestamp": 1606759744319482700,
				  "time": "2020-11-30T19:09:04.3194827+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-system:system/srl_nokia-system-name:name/host-name",
					  "values": {
						"srl_nokia-system:system/srl_nokia-system-name:name/host-name": "spine1"
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "bgp_neighbor_state",
				  "timestamp": 1606759744320286459,
				  "time": "2020-11-30T19:09:04.320286459+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:1:1]",
					  "values": {
						"srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "bgp_neighbor_state",
				  "timestamp": 1606759744320643010,
				  "time": "2020-11-30T19:09:04.32064301+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:0]",
					  "values": {
						"srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "bgp_neighbor_state",
				  "timestamp": 1606759744321328876,
				  "time": "2020-11-30T19:09:04.321328876+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:3:0]",
					  "values": {
						"srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}

				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "port_stats",
				  "timestamp": 1606759744321236615,
				  "time": "2020-11-30T19:09:04.321236615+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-interfaces:interface[name=ethernet-1/3]/traffic-rate",
					  "values": {
						"srl_nokia-interfaces:interface/traffic-rate": {
						  "in-bps": "87",
						  "out-bps": "71"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "bgp_neighbor_state",
				  "timestamp": 1606759744321422546,
				  "time": "2020-11-30T19:09:04.321422546+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:1:3]",
					  "values": {
						"srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
						  "admin-state": "enable"
						}
					  }
					}
				  ]
				}

				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "system_facts",
				  "timestamp": 1606759744323053620,
				  "time": "2020-11-30T19:09:04.32305362+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-system:system/srl_nokia-system-name:name/host-name",
					  "values": {
						"srl_nokia-system:system/srl_nokia-system-name:name/host-name": "leaf1"
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "port_stats",
				  "timestamp": 1606759744325290975,
				  "time": "2020-11-30T19:09:04.325290975+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-interfaces:interface[name=ethernet-1/36]/traffic-rate",
					  "values": {
						"srl_nokia-interfaces:interface/traffic-rate": {
						  "in-bps": "8646355",
						  "out-bps": "270083170"
						}
					  }
					}
				  ]
				}
				{
				  "source": "clab-t2-spine1:57400",
				  "subscription-name": "system_facts",
				  "timestamp": 1606759744327216712,
				  "time": "2020-11-30T19:09:04.327216712+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-platform:platform/srl_nokia-platform-control:control[slot=A]",
					  "values": {
						"srl_nokia-platform:platform/srl_nokia-platform-control:control": {
						  "software-version": "v20.6.2-332-gf3ab9e33e5"
						}
					  }
					}
				  ]
				}


				{
				  "source": "clab-t2-leaf1:57400",
				  "subscription-name": "system_facts",
				  "timestamp": 1606759744329930308,
				  "time": "2020-11-30T19:09:04.329930308+01:00",
				  "updates": [
					{
					  "Path": "srl_nokia-platform:platform/srl_nokia-platform-control:control[slot=A]",
					  "values": {
						"srl_nokia-platform:platform/srl_nokia-platform-control:control": {
						  "software-version": "v20.6.2-332-gf3ab9e33e5"
						}
					  }
					}
				  ]
				}

      ```    
      </details>
