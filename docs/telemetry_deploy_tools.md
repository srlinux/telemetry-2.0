# Tools container
- Any container with iperf3 tool is sufficient for demo purposes
  - The following project was used as an example (https://gitlab.com/Klepikov/docker_tools)
  - Example:

	<details>
	<summary>Click to see output</summary>

	```sh
	# cat 01.start_ubuntu_tools.sh
    #! /bin/bash

    # Basic verification for mandatory and optional arguments
        if [ ! $1 ]; then
            echo "[CRITICAL ERROR] Expected use of the script: 
        
        ./${0##*/} <name>
        "
            echo "[INFO] Mandatory argument <name> is missing"
            exit
        fi

        name=$1

        docker run -td --privileged \
        --sysctl net.ipv6.conf.all.disable_ipv6=0 \
        --sysctl net.ipv6.conf.all.accept_dad=0 \
        --sysctl net.ipv6.conf.default.accept_dad=0 \
        --sysctl net.ipv6.conf.all.autoconf=0 \
        --sysctl net.ipv6.conf.default.autoconf=0 \
        -v $(pwd)/tools:/tools:rw \
        --name $name ubuntu-tools

    # Create usable NameSpace
        pid=$(docker inspect $name  --format "{{json .State.Pid}}")
        ln -s /proc/$pid/ns/net /run/netns/$name

    # Populate /etc/hosts file
        ip=$(docker inspect $name --format "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}")
        sed -i "/${name}/d" /etc/hosts
        echo "${ip} ${name} ${name}.ipd.lab" >> /etc/hosts

    # Create DIR
        mkdir -p /run/netns/ 2>/dev/null

    # Report and keep track of running containers
        echo -e "[INFO] name=$name; pid=$pid; mgmt_ip=$ip"
        echo $name >> .running

	```
	</details>

- Newely created "tools container" has to be connected to a proper Linux Bridge
    - Example:
        ```sh
        bridge=br-leaf1-36
        container=tools-1

        # Create VETH pair
        ip link add uplink type veth peer name $container

        # First VETH port 'uplink' goes to a tool container 
        ip link set uplink netns $container
        ip netns exec $container ip link set dev uplink up

        # Second VETH port '$container' goes to '$bridge'
        ip link set $container master $bridge 
        ip link set $container up

        # Bring '$bridge' UP (just in case)
        ip link set $bridge up
        ```
- IP addressing and routing
    - "tools-1" (not persistent)
        ```sh
        # Add IPv4
        ip add add 172.31.1.2/24 dev uplink
        ip ro add 172.31.0.0/16 via 172.31.1.1

        # Add IPv6
        ip -6 add add 2000::172:31:1:2/112 dev uplink
        ip -6 add add 2000::172:31:1:3/112 dev uplink
        ip -6 ro add 2000::172:31:0:0/96 via 2000::172:31:1:1
        ```
    - "tools-2" (not persistent)
        ```sh
        # Add IPv4
        ip add add 172.31.2.2/24 dev uplink
        ip ro add 172.31.0.0/16 via 172.31.2.1

        # Add IPv6
        ip -6 add add 2000::172:31:2:2/112 dev uplink
        ip -6 add add 2000::172:31:2:3/112 dev uplink
        ip -6 ro add 2000::172:31:0:0/96 via 2000::172:31:2:1
        ```
- Generate traffic with IPerf3
    - Traffic is generated using multiple flows in order to achieve a proper balancing
    - "tools-1" is used as a server
        ```sh
        # Start Iperf3 server
        iperf3 -s -p 5201 &
        iperf3 -s -p 5202 &
        ```
    - "tools-2" is used as a client
        ```sh
        iperf3 -c 2000::172:31:1:3 -t 10000 -i 10 -p 5202 -B 2000::172:31:2:3 -P 64 -b 2000000 &
        iperf3 -c 2000::172:31:1:2 -t 10000 -i 10 -p 5201 -B 2000::172:31:2:2 -P 64 -b 2000000 &
        ```
