# SR Linux configuration

- IPv6 addresses are used on every fabric link
    <details>
    <summary>Interface configuration example (clickable)</summary>

    ```sh
    A:leaf1# info from running interface ethernet-1/1
        interface ethernet-1/1 {
            admin-state enable
            vlan-tagging true
            subinterface 1 {
                ipv6 {
                    address 2002::100:64:1:0/127 {
                    }
                }
                vlan {
                    encap {
                        single-tagged {
                            vlan-id 1
                        }
                    }
                }
            }
        }
    A:leaf1# info from running network-instance default interface ethernet-1/1.1
        network-instance default {
            interface ethernet-1/1.1 {
            }
        }
    ```
    </details>

- eBGP is used as a routing protocol
    <details>
    <summary>BGP configuration example (clickable)</summary>

    ```sh
    A:leaf1# info from running network-instance default protocols bgp
        network-instance default {
            protocols {
                bgp {
                    admin-state enable
                    autonomous-system 101
                    router-id 192.0.1.1
                    ebgp-default-policy {
                        import-reject-all false
                        export-reject-all false
                    }
                    group eBGPv6 {
                        export-policy export-local
                        ipv4-unicast {
                            admin-state enable
                            advertise-ipv6-next-hops true
                            receive-ipv6-next-hops true
                        }
                        ipv6-unicast {
                            admin-state enable
                        }
                    }
                    ipv4-unicast {
                        multipath {
                            allow-multiple-as true
                            max-paths-level-1 64
                            max-paths-level-2 64
                        }
                    }
                    ipv6-unicast {
                        multipath {
                            allow-multiple-as true
                            max-paths-level-1 64
                            max-paths-level-2 64
                        }
                    }
                    neighbor 2002::100:64:1:1 {
                        peer-as 201
                        peer-group eBGPv6
                    }
                    neighbor 2002::100:64:1:3 {
                        admin-state enable
                        peer-as 202
                        peer-group eBGPv6
                    }
                }
            }
        }
    ```
    </details>
    
    <details>
    <summary>BGP verification (clickable)</summary>    

    ```sh
    A:leaf1# show network-instance default protocols bgp neighbor
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    BGP neighbor summary for network-instance "default"
    Flags: S static, D dynamic, L discovered by LLDP, B BFD enabled, - disabled, * slow
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    +---------------------+-------------------------------+---------------------+-------+-----------+-----------------+-----------------+---------------+-------------------------------+
    |      Net-Inst       |             Peer              |        Group        | Flags |  Peer-AS  |      State      |     Uptime      |   AFI/SAFI    |        [Rx/Active/Tx]         |
    +=====================+===============================+=====================+=======+===========+=================+=================+===============+===============================+
    | default             | 2002::100:64:1:1              | eBGPv6              | S     | 201       | established     | 0d:14h:33m:54s  | ipv4-unicast  | [2/2/1]                       |
    |                     |                               |                     |       |           |                 |                 | ipv6-unicast  | [2/2/1]                       |
    | default             | 2002::100:64:1:3              | eBGPv6              | S     | 202       | established     | 0d:14h:35m:29s  | ipv4-unicast  | [2/2/3]                       |
    |                     |                               |                     |       |           |                 |                 | ipv6-unicast  | [2/2/3]                       |
    +---------------------+-------------------------------+---------------------+-------+-----------+-----------------+-----------------+---------------+-------------------------------+
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Summary:
    2 configured neighbors, 2 configured sessions are established,0 disabled peers
    0 dynamic peers

    ```
    </details>
- Client (iperf3) connections are distributed in the network using basic export policies
    <details>
    <summary>Policy configuration example (clickable)</summary>    

    ```sh
    A:leaf1# info from running routing-policy
        routing-policy {
            prefix-set local {
                prefix 172.31.0.0/16 mask-length-range 16..32 {
                }
                prefix 2000::172:31:0:0/96 mask-length-range 96..128 {
                }
            }
            policy export-local {
                statement 10 {
                    match {
                        prefix-set local
                    }
                    action {
                        accept {
                        }
                    }
                }
            }
        }
    ```
    </details>          

- Every SR Linux instance has to be configured with **gnmi server** and **TLS**

    <details>
    <summary>GNMI server configuration (clickable)</summary>

    ```sh
    A:leaf1# info from running system gnmi-server
        system {
            gnmi-server {
                admin-state enable
                timeout 7200
                rate-limit 60
                session-limit 20
                commit-confirmed-timeout 0
                network-instance mgmt {
                    admin-state enable
                    use-authentication true
                    port 57400
                    tls-profile tls-clab-t2-leaf1
                    source-address [
                        ::
                    ]
                }
                unix-socket {
                    admin-state disable
                    use-authentication true
                }
            }
        }

    ```
    </details>

    <details>
    <summary>TLS configuration (clickable)</summary>

    ```sh
    A:leaf1# info from running system tls server-profile tls-clab-t2-leaf1
        system {
            tls {
                server-profile tls-clab-t2-leaf1 {
                    key $aes$ayJZg0RibJxM=$5z8t35YnqJ35z8gVaMR56gPN7T5ZUHKziOlPQuEwdk5zQ7Tru5x7ZGW7/YbnzjJSrqrEoil25aYOlO7EqMZS8WlJxbTu+kwEaXx/pcNzAqKXyiKU+YrQuHJFwVijItc3oe6HhKyKmWxcSjd5ZOqQZHrz+euTP9hA9sLk6hByy/JcHBsgHu2LOTZ1qwUXXRTdpQFeko02N5Rz92iXXBsfYEy9Q47pHeZ1GeoW+oyaHvMM34/xKWlfIlgAfj032hdhYow1LZi9OfH9XRm/iT9hRojtLf9O9vivBuc+hceq/+EUh/TMnjPQr6eDMXN1OOPJtAeTBddtonzvp2agdeoAvML7QzlkUiITHFOzTSqPq7isbstFJ/ZKsInqbbsfJ4w+Vd/TMiLsEiS9GJ5bffP272zk+qAHwK37YOdIsAAQrRg003FSLp0Mwemd/VnLCMsFFreA8PB5UrMPXweAF5B6GDAHpd3Jvy8VFI5saKPanzsqtOGR+NG4um6/VEFD1qbf4nRdqO4YykCrghWZtmGR35NZ2h9HElG0hSlhs45q1XxCc1SyCmPAetGOh8TDSVAD3F39svDe0i7q3EFPAlgch89TxLdXeEMyLzq7uN1Sxp+GZSSf4kIHO4DTbqzVdejLIOqpqlBTvXD/XzovIR+CJ8vFZRTz3jhf2vuyn04Ro9k+Swrm2I7jo6RKbrx/ZqJTUQlBxsLXKCPNV/4+Elt05Dqhv0IyE6z6ALVobCGUtjC9/PBMYjzRl/OgPPZRhfhNrGdhWllwi/DOdlGcq1BSeo7gdcxVzKTa/yC0fYD0PJoDEAWO0VCLs4x2C/6Exr3zv8xCqLz4tMUu7pwpE8Z1IJJ0JfOSKHTcUPPl8MMLUQx7jEbXWKr4Yy8lKtEMbT0yX/u2dU84yiyxXTe8Qfhw0StPtv8pjEVwM6hXWj8IKgaZ0s5loyH+0LveVKUV/Q6o0wAUT0AefSnOJk/7J4AW4rkW2PvkWCJCzeLLwmePd3TDgMGYqdgd3D2AxXTyswLc5VE25eMxKXIUZmDASRxzAAb+iKj69VLbn8UbH53VpxT+PtX/BoSXwsR/y/8794+yXmDdCvuT5BcgCdVy8MWcXDiQI7g6BnVqzz2ErGskPT/4RqiCBv5oCI6TBGV1XuvPene33RP9zjQYLbVj3s9F83C0OPHUn4jf9bml9Fe+BDiSjM8vHIV4o6AmifLPaD9X8zbGfLvR57JOhyoRRBKLYSru2GXxPdKgslD/bYHkkgKTpnCQ5MfQ/cGghV15esleeW5ZpKa6dk+REUBqd9gHRBoeyr4tipODyS+9gWf5MHeBl0KrK1oPTvGBX4evKWmk5g/ESa1NFCdToiOFZrNNPTy7WRp2X8kMUwfyhNKRqIw0sHBRrnI0GDFS00jgvFhZ0Q3z1lRqGFULfi+56frbTI/wm4IZrNPbcAaOi2/4nfjM7cRaZbzHYLDI+d4IGHGL4mAOIOHgeHwtNFtK7D0ANWxtuSr+7jet5LD2AY2sJbWAlBbu8UbcVAT9PO85Y9i5F2hRmp2RLzQdjYEyfgsKmj1HYvwjXyVS0GrlaE3TEslas1EfpniWz5fmZfsJC+dT0gmMawPNJCGuZ8UTzmVEWyKU8TvQV7tDnYAox/pZhtw0E5I98KItf9Q9X8+7XQIZNz+6ULlLH/ahkhNh/wYP+e2ymsKHAydAG5K4i/JhHoiyxRNgKM9IKQEvj9dN+bgzDM4AyCqwcCobYdKKAh8YikgvOXNFloCQV19hxY6iLODywEQSTAs+lE1rhE4RsMV6GYmIj5fpGqGo+mWIU+eh67nB2oK1PxTgW6TaB4lrn6hhDO60JjiN8YkFpXn/MBrZpoB0+VhFd7dhlAMgkbDao5wPvPYlWfpdBznvbEkFtrY38oYPtatbFAxcMr2ykFGyYi+c9h01RqV1F0jHZKfh6CgOfYYLOj/fYzKb0t33wh7Dg5UU7jc8UAyd9LYKo5UnxvXD36a3AQ/D4yuYl72ROtYPer2C/N1xn39y3fPx4GTAo9n7IhzpIVvuuQPrgFgL5iplBjxp8yDcrhwXatNgU0HoR/mzx+uYl0nGDL7+ReFBtx1CtVjmoEArdMuY+v7Ek7yPwqyp09KlNWxac6ZAMVUkPwb4tKkHwaOrYW/sAqWT+f6qtv0+/1DZI3wnMsWg5zTDYNaQXm+8TkQlIqxs+oM4u5q9W5PvPFuO8FddGGKB931hBWogzZcqRMfQ/pfifDFER5jJekVCVB9ayszb0SuYdl2qGlQkW3TB6SnwHmQVB/q8jzOcttNhOxZBvZFLsfv78wvNaBamdCN5GHFRnY8jxR397VparjK9muGypUIwRMImh/Krqes1Tl8sunA8DjvqIuq4WGU3ySGV7bjVsgaYALjjqlq1s+KuSIV70oLHmGq3VtDSe+4QymiBxq68ZeuJDj1CXs0+qIk0jP2uE9VJ7cjwea7bsSo6HdJJuzwe5jQNVQ5Kks7lLbfFc6iRyP6NYheqL0rHcoZVUpy0YzKwXYiiblsHS9NNAEtDjR4r8d6FRcSaonSkRo1GczNHWwggPORH6JUiqIjvdpRp2DpVG+ahNf9YFDwklaLjzLQKiHE/8vS3xtxDiQV9Y+c+Pihly6RSjcoUi8WtaDVSKJJTjucrOVsqkkz27m7olZfx6GCCaJBrkEexT3QeZkJ5YXg+9OPOjn4kQadrSZRcSSK6dEsjx/kxQf/xo6AdzRgGRgGR3ym1pcX6qC3XBjl10CxACiPY0NEakn/g7Eg8lqZ+PbPiDWcnhtUwzVwaE4XzRTUrkGrOPaAh5To4kj/U7MlHn4V42d9c/6HZ9HCIouxsq5obzMFBjkpZuy+O0l+A61XViBcyWcRdG0iagPJhljTs8EQEF2Ah3L3fqHK5TpebomHGsAURRUztVEAitd3M+XsHU2IjHrLhE98ruKRbzIARHlxv5PgApfJw9VLLapSWHdbQ8HeWLxnJgDNlet74ManvS/TyGqhcIXyyRfTOs1dBxsgDtYpj68sfBoe0YxZnqT/R1cVUFmgizSxJeY4bkxywW26R3e1rzgrJil03Y2s/DvE8sdZ/MPCMnW8ZfOcmsbsUiLGF3f/POY1+zlOIv3C2GNpAMCZsAAQxDwJciNMwr8hPmaAWRatu2GDZzErbgqHZK70IRFwTXaBHkawfEavYF0GYhOg4AOf/LnuQ5H+eyaNsZScMwDL/HsXksp/2KP93HXhFZfcx5JJfVSqfJkUve3VZzbNPfO9WXgvshdh9F6rDWphnBz8IWAR6yr9htptSNDwnBk8WwncpVXx2deAovxEVVvLI9d6O6gC6EmZI7Ev3TKO/jNrUeIqIwveQDmitqqr3tkr+aggITMebpqjrVEizmcYxrOQ/CGiYQF/c0Vg4cbj1Rg9GOOOJEfExrqgy5MTaF5+o+99R08RBsj9kMxtbBBgyNoEhmkJaNIy1I+pX0xJSyuDp532DUiT33d7BFHb8w8zL32NPPS6G50m4cdVlSpAcLMvLciiUgRXTyeJtZYIKLsdg4n4p+nHYdfZSq75XJHstKuve1VJgugsrf+B+jNq84OqT9HBR6QsJCVEd0yK2FcQ/TRXRn21U3vVZ8eJsqglqVxJwk8Oo3xlcAnOS2tgdw7VbjtE/XN0iXWG4KdFPkXGz8DPXmHktXe0EvVYxJDxG4nRiMLGYRZUhL+WPdl2/GDa1gX8aDAkMGRTFehV3GDaSRZYe3MaXrtfN66PzhIGGNsjbKT2PztWJll2ejSOPp5S2L6gM8goJUhAvZnKngEX3zvVf9GKHiXAQ2VolcC+2hOXk8OKFZ4khIWroK4sNYx6DlDu6SdkXq1ANnjjSRqUbZ1/iX4VfGPRY1JDbtqKnx1kX3BgyWcSXr2LdSHP9tUHUWXlQJdsRGT2+85QmOlEBWzK79/onm4mLMjvThYSz7A3X+9rlnPUtHndLtj9OYzJgITnXaSw2nl3qwPm7ICVNZRNPs5Rm6CfWbHxp+C9TCVWgWHZKF23cyJnGiWSimmhtPVL0n/y/qQ6CIANPVMtCAYMfgXPTCpI0OlmdFF0d1AXcjRwCIFiY/ZKZmdVd4VioihAQhv2fPQVi7/jOH5EcYg912TfFhQRabWhjw+50v6wdD1JmMiiQB176wrSXfoIe25Hg9OaDTb0dj3NtX9hf1ASUUDka7UYkLT0BnhP74FmsZTH9kUVrJJjyEVJ/tQEz74IRBsAoIkkywAGaglEB9qP501HHCBKSBtfmPC5YJA6k9htXC1HIfKn90i1JV6H1te3T9evg20PdnQAYQsNsgckv30z+Fp8asL1G8L5UcejNNzk=
                    certificate "-----BEGIN CERTIFICATE-----
    MIIHFjCCBP6gAwIBAgICEAkwDQYJKoZIhvcNAQELBQAwcjELMAkGA1UEBhMCQkUx
    CzAJBgNVBAgMAkZMMQ4wDAYDVQQKDAVOb2tpYTEMMAoGA1UECwwDSU9OMQ8wDQYD
    VQQDDAZzdWJfY2ExJzAlBgkqhkiG9w0BCQEWGHBhdmVsLmtsZXBpa292QG5va2lh
    LmNvbTAeFw0yMDExMjcxNTQxMjJaFw0zMDExMjUxNTQxMjJaMIGNMQswCQYDVQQG
    EwJCRTELMAkGA1UECAwCRkwxEjAQBgNVBAcMCUFudHdlcnBlbjEOMAwGA1UECgwF
    Tm9raWExDDAKBgNVBAsMA0lPTjEWMBQGA1UEAwwNY2xhYi10Mi1sZWFmMTEnMCUG
    CSqGSIb3DQEJARYYcGF2ZWwua2xlcGlrb3ZAbm9raWEuY29tMIICIjANBgkqhkiG
    9w0BAQEFAAOCAg8AMIICCgKCAgEApbCta48Vn1UpUfvDSZ5lqlruIHLIzi/GHxF2
    dtrwOgH2LfYswZ0sB0K/TP++McnIIvrgNKieSo+IiJKljrVwVbYBwO7ssHWD2pZ6
    U6e994rK/+LviMj7qD3/Tw9AOXUqsAB35YQHwBR7MAAs7V/y3kJ4zcMVrSwUHzC6
    mYKhDqX1LR7gp78u159eOTS2eaPLk1ZUVP/zX/JI/cU1qDZn880KscT3xpZt94XB
    1d+pW9MUwyAJqfm60bNFkKHyJBwX2Cmc+xK4BeqMiddIA1PicWNULzoZxymJAB1L
    CZl3/OFK1j8uy90yPmaAQIlPbR73zXLqmx/0HxjVSKqZah8sWsbUMn5Nw3wmUegv
    Kpznt1AMhTiOHdDfsmoq+5Fz1pBN0jV0V6AH7FZ210MIB32CHoaWBihlNXVG2Q0B
    b5ZNQLV+jYdtTCnBjcrOtVJSFdI7M50uDWpT0fz2kTtMPz/kSkia7W8ODhpVW0iq
    R3udCfmeCMZVzhVEFjlsrx0fmO5NPcRyYKKU815LFoWcGuJVtPK6GwNTV51pg6kA
    y2q61kmytTF+zgqZlVWawXJRsOEG6bbQawaWQ81SYH1ZRNK4tJg0trXSIwiX1eY2
    BZslT2Fklfv2u1G5nJnv1HjE6ehmiXGVyD2nyOuLlTW6KHnFQ0lIT7DMeZZ1Yygo
    UoE6c2UCAwEAAaOCAZgwggGUMAkGA1UdEwQCMAAwEQYJYIZIAYb4QgEBBAQDAgZA
    MDMGCWCGSAGG+EIBDQQmFiRPcGVuU1NMIEdlbmVyYXRlZCBTZXJ2ZXIgQ2VydGlm
    aWNhdGUwHQYDVR0OBBYEFKTXC/dJ968hcwITznUUiTVXWy4fMIG1BgNVHSMEga0w
    gaqAFPBJ96U4Sr9LGBrpgq2OcnkC2wjPoYGNpIGKMIGHMQswCQYDVQQGEwJCRTEL
    MAkGA1UECAwCRkwxEjAQBgNVBAcMCUFudHdlcnBlbjEOMAwGA1UECgwFTm9raWEx
    DDAKBgNVBAsMA0lPTjEQMA4GA1UEAwwHUm9vdF9DQTEnMCUGCSqGSIb3DQEJARYY
    cGF2ZWwua2xlcGlrb3ZAbm9raWEuY29tggIQADAOBgNVHQ8BAf8EBAMCBaAwJwYD
    VR0lBCAwHgYIKwYBBQUHAwEGCCsGAQUFBwMRBggrBgEFBQcDAjAvBgNVHREEKDAm
    ghVjbGFiLXQyLWxlYWYxLmlwZC5sYWKCDWNsYWItdDItbGVhZjEwDQYJKoZIhvcN
    AQELBQADggIBAHoDQc5B2iQyd6mT+ebhHGpS0VXMlOd9GEuSpsdZX86UebjhM0t1
    5hYy98cfHSJEVMp7HXLOArvkM01bsnghRCxDiUEWw+641DsmBoiwCrrZL0Z5iFSW
    JTcLIi1c+cJvqwpXKa2Yi65DeNfM7IodsvPXDw/I7K5BOulzV22lw+uLB/2gs39G
    sGOJYaNrNukr6b8zYtcIK/UVeSq+RThlMUOAYeQyQbGbqdRBscIbZGReN4AXxgZG
    ZYIaz/b8uM/Jiv96/yce/DrshLgsAjTYmI9IhWyR0gT/3A45jIsz7nY3JFMFCQWr
    Dw/RYDTq2cYAoGPTrhclH/zbcWd+jh6KES2hoURN4xvjAwdDr3dCGtbCPMYnuy9p
    gst1+6KWx9Lakg8dRpxwkX03N1krHzAuX8LKxdUHZl38BrHWL8gM2nAx3523hFEF
    +UZfWIe5U60v1H7uMJB46VqdXIECcJb+baituw74q8Z1bJyOiFomZSbu94itRPUM
    BHBjCPVhbv78Eivyob26Gjxp0gO4UK8NqgHQCEmFoBEz4ikxoRmqFWbEa2i000H+
    6U8ZZS9lfFbbOVCueVzadZpEE0r4ERnMwHe42jTH9oeHeS0dzqnk2mAgWOfrS2c+
    xlinXCPXZnr055QOxTqApxg6QBJAQgrkU8Rr75QEAGU6nxab1iE7Y74s
    -----END CERTIFICATE-----"
                    authenticate-client false
                    trust-anchor "-----BEGIN CERTIFICATE-----
    MIIGATCCA+mgAwIBAgIUFjSq1GO1JJ0Tn/TEp+oQKX2/iUAwDQYJKoZIhvcNAQEL
    BQAwgYcxCzAJBgNVBAYTAkJFMQswCQYDVQQIDAJGTDESMBAGA1UEBwwJQW50d2Vy
    cGVuMQ4wDAYDVQQKDAVOb2tpYTEMMAoGA1UECwwDSU9OMRAwDgYDVQQDDAdSb290
    X0NBMScwJQYJKoZIhvcNAQkBFhhwYXZlbC5rbGVwaWtvdkBub2tpYS5jb20wHhcN
    MjAwOTE3MjAyMTE4WhcNNDAwOTEyMjAyMTE4WjCBhzELMAkGA1UEBhMCQkUxCzAJ
    BgNVBAgMAkZMMRIwEAYDVQQHDAlBbnR3ZXJwZW4xDjAMBgNVBAoMBU5va2lhMQww
    CgYDVQQLDANJT04xEDAOBgNVBAMMB1Jvb3RfQ0ExJzAlBgkqhkiG9w0BCQEWGHBh
    dmVsLmtsZXBpa292QG5va2lhLmNvbTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCC
    AgoCggIBAKTNwiSEgz6Oh6BXR7Y6R0yZpsXJocCVZxBlUqMpq7B+PcswqJGL3pwI
    4R2bMncCs7xKpPjtA1Z6ZF3wFaXXeYfYH1ayMd/emR31dLhMyCyf8OHLfRUQQGRA
    GRddvk0mtvOwQvGQimUSEIeIiz4uQnQHT/P5zTy6XqraETnbfLsoW01MmYlycThC
    CzBv9AUmvzDKhL/sHU6i/w1TgomQorxKprsKnnZQBoj9ZO/lA8gMVyl21tq/D/4w
    Pmd0YKUa8C22AWQiUaYfuc7b+4R5/JNwDilo6AW0GF5r8VDkIZLgSK9HwEOJYq4t
    AHM5FgnQfrsGSiEBfLk1aJXB5WtnjgZhVKI/vpXRABXMZKDiqNHTAK6f+iYE7pZ2
    8RCT/A/NGFC4XkAeMxsUwvs6zMFIjxHR/cz/C+AIkNcCOUFiaA2mQgMF2LXiLWX7
    OZTPtjnkBG/fcTniY/6yFvxxHsL8LILGN2lrhxQqdedybSVtVDKOWj1kzo5vhVg+
    5jrFQ3LlzbLFyMg2CvTWAbsKlrOD+WaByxt087jjOVGT5LY8C8P7gUxfNtb91dxC
    CntDNri6ZlH54wjNq54eU55p1+LEcsF4DYFjvuTTU5UYq9cqjZQtpBX3lrxmujPv
    df/PNq+tkDykW81fV7f1QfLGyvH7QMf9BfMWk5sZL4hsfW6h3uP7AgMBAAGjYzBh
    MB0GA1UdDgQWBBRGJpzTmU+15fMNlRQ9ew5tfFVnkzAfBgNVHSMEGDAWgBRGJpzT
    mU+15fMNlRQ9ew5tfFVnkzAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIB
    hjANBgkqhkiG9w0BAQsFAAOCAgEAO99eGvTzgAjCFu2uxltK/J0Q8cffgDZ9S3+7
    vY1UBzZwiQRm9ojLfn9HB0TuspDu6F4nwiSUQy+ayncgeOm9fevjx6FtTEdU0nK2
    iPOuZmrid3ZrO4RF5xqaygaSBvTZwxyl1OupoZRvZFF5M5UuVE2/YpvTgfhN0SMl
    r6ak9bUvEp2K31DCK+bfe/+ZzJDYdeUhZ5Q+9x03BZp3gj8kPocn828wr6U0ylYz
    MvaglMBaXIILixa1gxuph9a4kJPXWcJNvw3qEBHsq1LWE19aJWABMxA6vfz9u8RG
    o5zHE1AKm7mRowhhBukzGrSO8jkJVZ6yKhVcGcGNiqm0YURoQmdCCAEHFKte7Dc8
    RjszmXp0JZ8e0Fp6A5BfAbeEF80AacbBBv2Eo933ZXB7RYsp80qk2jDpyueexTyQ
    I35IC17jFopZ0a2udIkyXvPfV2CSZ/DzH56LVqJRSpoWhumSziUoDuPioVUTsfG3
    QUeluXz7bmqTW8ymDfQUYxCCaJ0UDOFGzahT8YUVJq+hEnFe7oJPZluXAkZkxvJq
    UFkoi4IciMfZ02pCfHlp5A2OxHVKh+khfwFKKcefHh5LLPJQA0smMVfM/JSFGwbw
    RMu7Zba5Oroj/ToRKBhhj89uGp76mcZLW/K8A5S1e+C4lbe5QotgDs0z5NBJP0Mq
    tUonvrI=
    -----END CERTIFICATE-----
    -----BEGIN CERTIFICATE-----
    MIIF3DCCA8SgAwIBAgICEAAwDQYJKoZIhvcNAQELBQAwgYcxCzAJBgNVBAYTAkJF
    MQswCQYDVQQIDAJGTDESMBAGA1UEBwwJQW50d2VycGVuMQ4wDAYDVQQKDAVOb2tp
    YTEMMAoGA1UECwwDSU9OMRAwDgYDVQQDDAdSb290X0NBMScwJQYJKoZIhvcNAQkB
    FhhwYXZlbC5rbGVwaWtvdkBub2tpYS5jb20wHhcNMjAwOTE3MjAyMTE5WhcNMzAw
    OTE1MjAyMTE5WjByMQswCQYDVQQGEwJCRTELMAkGA1UECAwCRkwxDjAMBgNVBAoM
    BU5va2lhMQwwCgYDVQQLDANJT04xDzANBgNVBAMMBnN1Yl9jYTEnMCUGCSqGSIb3
    DQEJARYYcGF2ZWwua2xlcGlrb3ZAbm9raWEuY29tMIICIjANBgkqhkiG9w0BAQEF
    AAOCAg8AMIICCgKCAgEAvIS/HECMf0BJgJE6p4/e6W/qyw8dEhVpeQ4pnWXEgXI0
    v3s/5FbXCfMy3oeDwVkDM4gSXSgn5x+WmgSYRuCYPm2V4Zgy7pU2e0bTh9c4LCgC
    jsdoxEj9E85dFTYRIv1p73Im6kpJg8K9kSJnN6q7n6LkclO7TxoIbW6PwpNkBrvp
    nh9CXMyqmccSil1NGmdIesKf3tcBriQ3YvGPo23ggukv5tpuFVVH9vkm/wik0n3z
    6EXpihgJBFcI1C1c3PnIMooQemGIT7jLoj8ijfFcfs6lJdTgDPl3GCUwLFC6JkWg
    oIlaQCVXxshiWqEsG2J6NRcEjd6gs6loOV/fZ2hqp3Ah1Kn5kr+47vTr7SHzVG81
    9oDXKO8MUyKov02iab3J3m7MagJWjGfq+nCxFffnke6t4fZRpRKMGiLe3maaCH1n
    INwXxTdPsj+gysrfi259BFNh94DGtrgj2E5aE3CqH1ZY6HQ6qzMX7MJah+hJXHR1
    DGL5hT/7FT0bBerdzOo5VQarSvbYDbB8zucG3z2ja0YTMN7hetuDY268R6Ryalfm
    rGMGY06umFZRnbLc+uE1/035UkF2k7oSpKFakRShX5hMhtXBMQukpG8FELaBM3Pg
    oOsy8a7qB9xoUAhhOvb5MoPM2WBigoM0AwVxKmNFfmPJNeVrXOK948Tw/pPzWN0C
    AwEAAaNmMGQwHQYDVR0OBBYEFPBJ96U4Sr9LGBrpgq2OcnkC2wjPMB8GA1UdIwQY
    MBaAFEYmnNOZT7Xl8w2VFD17Dm18VWeTMBIGA1UdEwEB/wQIMAYBAf8CAQAwDgYD
    VR0PAQH/BAQDAgGGMA0GCSqGSIb3DQEBCwUAA4ICAQAHRCq+lQcDa/KgOWU5OyoP
    KfrzYeZpAt9EXMsKIjG3+mx7AwYULpcnshgVZ3gGoDpC5+RLvQhzMPP+t+fb2Pxn
    NFlYcRkeaFv7yt5UmzaMRDNHo43McuNSbau5Cmtpg4sh0BVDPujCNqF2iI96TBjX
    JtGV1ntS8/3TGyX1n+PB9YryHjJPloxf0r4Xj98NRwUfJQCLrX/24yEPak2sjjsp
    7XO1ngjrdwgUv7rktSncrirFj0nU0rCCUA6nLAZ+OPAcWRzhozKzhk174gfT2Lqc
    eS/dCUW34N1Hoh++2vhJmXp9OV6/6IzZtZenEk78pw4DqJPAimjq0HYX1PHCwhOY
    ZZrXOwz4BtHyYT/jt8fUdvL+N5bN3GJumOlQlFgMoAIY6/LKteZL1XBwy7vgwoGI
    HK6hFn32WJHEZXxw4FZE6lGgjbnVT8dieDzVtOpsAMJx/783kaFZO+F47/pgfHLu
    K7eD2+TQTQgpNCaGwMb3ypyDPYq9o+fqDQyPfDzAMolxgqnvtldoIUTJiM5cTnj+
    Pa1pHTTOCFwbIm7hM96CzanMzBo+XbgsPot6KKutKnFZlxOubRaB8ZLF1RR1cr2W
    eEgaQUKCLaJG7K+kTyih60c7a+rwk9feQMdltCDZL06jrz5Qgo6e730vkCaOvhTu
    CuCdnkncbWwLXi4iOEOopg==
    -----END CERTIFICATE-----"
                }
            }
        }

    ```
    </details>
