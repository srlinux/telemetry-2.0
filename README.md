# SRLinux Telemetry demo
## Introduction

This project is a work in progress to demonstrate a basic telemetry functionality for Nokia SR Linux products.

## Requirements

Host system 
  - Centos8
  - Centos7 wasn't tested

Internet access is required to pull docker images.

The following tools have to be installed on the host system:
  - docker-ce
  - containerlab version 0.8.1 (https://containerlab.srlinux.dev/install/)
  - gnmic (https://gnmic.kmrd.dev/#installation)

The following docker images are used:
  - SR Linux (provided by Nokia)
    - license.key file (provided by Nokia)
  - Telegraf
  - Prometheus
  - Grafana
  - (optionally) any Linux container with "iperf3" installed to generate beckground traffic
## Topology
  ![](docs/images/topology-2.0.png)
## Preparation phases
  - [Deploy SR Linux fabric](docs/telemetry_deploy_fabric.md)
## SR Linux configuration review
  - [SR Linux configuration](docs/telemetry_srl_config.md)
## Demo phases
  - [Telemetry: SRL local CLI](docs/telemetry_demo_cli.md)
  - [Telemetry: Linux tools (gNMIc)](docs/telemetry_demo_gnmi.md)
  - [Telemetry: Telegraf/Prometheus/Grafana](docs/telemetry_demo_grafana.md)
